﻿using EL_Online.Web.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EL_Online.Web.EL_OnlineServiceSite;
using EL_Online.Web.EL_OnlineSeviceEndUser;
using EL_Online.Web.Helpers;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using System.Configuration;
using Antlr.Runtime.Misc;

namespace EL_Online.Web.Controllers
{
    public class DashboardController : BaseController
    {
        // GET: Dashboard
        public ActionResult Index()
        {
            var dashboardModal = new DashboardModal();
            int endUserId;
            try
            {
                endUserId = Convert.ToInt32(Session["EndUserID"]);
            }
            catch (Exception ex)
            { throw ex; }

            // check Session
            if (Session["StrID"] == null || Session["StrID"].ToString() == "")
            {
                return RedirectToAction("Login", "Account", new { area = "" });
            }

            // Data
            try
            {
                if (Request.QueryString.ToString() != "")
                {
                    Session["SiteID"] = Request.QueryString["SiteID"].ToString();
                    Session["HistoryID"] = "0";
                }

                //Data Set user details
                GetEndUserDetails(dashboardModal, endUserId);

                // Data Set current sensor details
                GetSensorDetailsLastDay(dashboardModal, endUserId);

            }
            catch (Exception ex)
            { return RedirectToAction("Login", "Account", new { area = "" }); }


            return View(dashboardModal);
        }

        private void GetEndUserDetails(DashboardModal dashboardModal, int endUserId)
        {
            DataSet endUserInfo;
            using (var endUserService = new EndUserSoapClient())
            {
                endUserInfo = endUserService.GetEndUser(Session["strID"].ToString(), endUserId.ToString());
            }
            if (endUserInfo != null)
            {
                var userInfo = endUserInfo.Tables["tbl"].Rows[0];
                dashboardModal.ZipCode = userInfo["ZipCode"].ToString();
                dashboardModal.SiteName = userInfo["Name"].ToString();
                dashboardModal.Note = userInfo["Note"].ToString();
                dashboardModal.City = userInfo["City"].ToString();
                dashboardModal.Address1 = userInfo["Address1"].ToString();
                dashboardModal.Address2 = userInfo["Address2"].ToString();
                dashboardModal.Objektnamn = userInfo["Opt1"].ToString();
                dashboardModal.Debiteringkod = userInfo["Opt2"].ToString();

            }
        }

        private void GetSensorDetailsLastDay(DashboardModal dashboardModal, int endUserId)
        {
            dashboardModal.SensorCurrentDetailList = new List<SensorCurrentDetailsModal>();
            using (var endUserService = new EndUserSoapClient())
            {
                var dsTmp = endUserService.GetSensorValues(Session["strID"].ToString(), endUserId.ToString());
                var sensorGroupList = (from DataRow dr in dsTmp.Tables[0].DefaultView.Table.Rows
                                       select new
                                       {
                                           SensorGroup = dr[0],
                                           SensorGroupId = dr[1],
                                           SensorId = dr[8],
                                       }).ToList().OrderBy(x => x.SensorGroup);

                var tDate = Convert.ToDateTime(DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day);
                var fDate = tDate.AddDays(-1);

                DataSet dsSensorHistory;
                foreach (var it in sensorGroupList)
                {
                    dsSensorHistory = endUserService.GetSensorHistoryPeriod(Session["strID"].ToString(), "9", "1440", endUserId.ToString(), it.SensorId.ToString(), fDate.ToString(), tDate.ToString());
                    var avgUsage = dsSensorHistory.Tables[0].DefaultView.Table.AsEnumerable().Select(x => x.Field<double>("Avg")).FirstOrDefault();
                    switch (it.SensorGroupId.ToString())
                    {
                        case "100":
                            // El
                            var sdlEl = new SensorCurrentDetailsModal();
                            sdlEl.TypeName = it.SensorGroup.ToString();
                            sdlEl.Value = Math.Round(avgUsage / 1000).ToString();
                            sdlEl.SensorId = it.SensorId.ToString();
                            sdlEl.UnitName = "kWh";
                            sdlEl.IsElectricity = true;
                            dashboardModal.SensorCurrentDetailList.Add(sdlEl);
                            break;

                        case "110":
                            // Cold water
                            var sdlCold = new SensorCurrentDetailsModal();
                            sdlCold.TypeName = it.SensorGroup.ToString();
                            sdlCold.Value = Math.Round(avgUsage / 1000).ToString();
                            sdlCold.SensorId = it.SensorId.ToString();
                            sdlCold.UnitName = "liter";
                            sdlCold.IsColdWater = true;
                            dashboardModal.SensorCurrentDetailList.Add(sdlCold);
                            break;

                        case "120":
                            // Hot water - formula for conversion !!
                            var sdlHot = new SensorCurrentDetailsModal();
                            sdlHot.UnitName = "liter";
                            sdlHot.TypeName = it.SensorGroup.ToString();
                            sdlHot.Value = Math.Round(avgUsage / 1000).ToString();
                            sdlHot.SensorId = it.SensorId.ToString();
                            sdlHot.IsHotWater = true;
                            dashboardModal.SensorCurrentDetailList.Add(sdlHot);

                            break;

                        case "130":
                            // Heat
                            var sdlHeat = new SensorCurrentDetailsModal();
                            sdlHeat.TypeName = it.SensorGroup.ToString();
                            sdlHeat.Value = Math.Round(avgUsage / 1000).ToString();
                            sdlHeat.SensorId = it.SensorId.ToString();
                            sdlHeat.IsHeat = true;
                            sdlHeat.UnitName = "kWh";
                            dashboardModal.SensorCurrentDetailList.Add(sdlHeat);
                            break;

                        case "140":
                            // Temperatur
                            var sdlTemperatur = new SensorCurrentDetailsModal();
                            sdlTemperatur.TypeName = it.SensorGroup.ToString();
                            sdlTemperatur.UnitName = "grader C";
                            sdlTemperatur.IsTemp = true;
                            sdlTemperatur.Value = Math.Round(avgUsage).ToString();
                            sdlTemperatur.SensorId = it.SensorId.ToString();
                            dashboardModal.SensorCurrentDetailList.Add(sdlTemperatur);
                            break;
                        case "105":
                            // Floor heat
                            var sdlFloorHeat = new SensorCurrentDetailsModal();
                            sdlFloorHeat.TypeName = it.SensorGroup.ToString();
                            sdlFloorHeat.UnitName = "kWh";
                            sdlFloorHeat.IsFloorHeat = true;
                            sdlFloorHeat.Value = Math.Round(avgUsage / 1000).ToString();
                            sdlFloorHeat.SensorId = it.SensorId.ToString();
                            dashboardModal.SensorCurrentDetailList.Add(sdlFloorHeat);
                            break;
                    }

                }

            }



        }

        public JsonResult GetStatisticsByDateRange(string sDate, string eDate, bool isCompare, string iSensorId)
        {
            if (sDate == null || eDate == null)
            {
                var cday = DateTime.Today;
                sDate = cday.AddYears(-1).ToString();
                eDate = cday.ToString();
            }
            var cookieCulture = Request.Cookies["_culture"];
            var endUserId = Convert.ToInt32(Session["EndUserID"]);
            var fDate = DateTime.Parse(sDate);
            var tDate = DateTime.Parse(eDate);
            tDate = tDate.AddDays(2);
            fDate = fDate.AddDays(1);
            var chart = new ChartModel();
            var lastYearFdate = fDate.AddYears(-1);
            var lastYearTdate = tDate.AddYears(-1);

            try
            {   
                DataSet dsSensorHistory;
                DataSet dsSensorHistoryLastPeriod = null;
                using (var endUserService = new EndUserSoapClient())
                {
                   
                    dsSensorHistory = endUserService.GetSensorHistoryPeriod(Session["strID"].ToString(), "9", "1440", endUserId.ToString(), iSensorId, fDate.ToString(), tDate.ToString());

                    if (isCompare)
                    {
                        dsSensorHistoryLastPeriod = endUserService.GetSensorHistoryPeriod(Session["strID"].ToString(), "9", "1440", endUserId.ToString(), iSensorId, lastYearFdate.ToString(), lastYearTdate.ToString());
                    }
                }

                // Selected Date Range
                if ((dsSensorHistory != null) && (dsSensorHistory.Tables.Count != 0))
                {

                    if (dsSensorHistory.Tables[0].Rows.Count != 0)
                    {
                        int iCol = 0;
                        var dataSetForSelectedPeriod = new List<double>();
                        var previousYear = fDate.Year;
                        var monthList = new List<string>();

                        if (dsSensorHistory.Tables[0].Rows.Count > 60 && iSensorId != "586")
                        {
                            var ml = (from DataRow dr in dsSensorHistory.Tables[0].DefaultView.Table.Rows
                                      select (DateTime)dr[0]).ToList();
                            monthList = ml.Select(x => new DateTime(x.Year, x.Month, 1).ToString()).Distinct().ToList();

                            var gruopData = from b in dsSensorHistory.Tables[0].DefaultView.Table.AsEnumerable()
                                            group b by ((DateTime)b[0]).Month into g
                                            select g.Sum(x => x.Field<double>("Avg"));
                            foreach (var item in gruopData)
                            {
                                var calItem = 0.00;
                                calItem = (int)(item / 1000);
                                dataSetForSelectedPeriod.Add(Math.Round(calItem));
                            }

                        }
                        else
                        {
                            for (int i = 0; i < dsSensorHistory.Tables[0].Rows.Count; i++)
                            {
                                double tbl0_V = 0.0;

                                DateTime dtX;

                                dtX = Convert.ToDateTime(dsSensorHistory.Tables[0].Rows[i]["StartDT"].ToString());
                                previousYear = dtX.Year;
                                tbl0_V = Convert.ToDouble(dsSensorHistory.Tables[0].Rows[i]["Avg"].ToString());
                                if (iSensorId != "586")
                                { tbl0_V = tbl0_V / 1000; }

                                tbl0_V = Math.Round(tbl0_V);
                                if (tbl0_V < 0)
                                {
                                    tbl0_V = 0;
                                }

                                dataSetForSelectedPeriod.Add(tbl0_V);
                                monthList.Add(dtX.ToString("G"));
                                iCol++;
                            }
                        }

                        if (cookieCulture != null && cookieCulture.Value == "en-us")
                        {
                            if (iSensorId == "4269") chart.Series.Add(new Series() { name = previousYear.ToString(), data = dataSetForSelectedPeriod });
                            if (iSensorId == "4267") chart.Series.Add(new Series() { name = previousYear.ToString(), data = dataSetForSelectedPeriod });
                            if (iSensorId == "4268") chart.Series.Add(new Series() { name = previousYear.ToString(), data = dataSetForSelectedPeriod });
                            if (iSensorId == "1659") chart.Series.Add(new Series() { name = previousYear.ToString(), data = dataSetForSelectedPeriod });
                            if (iSensorId == "586") chart.Series.Add(new Series() { name = previousYear.ToString(), data = dataSetForSelectedPeriod });
                            if (iSensorId == "1604") chart.Series.Add(new Series() { name = previousYear.ToString(), data = dataSetForSelectedPeriod });
                        }
                        else
                        {
                            if (iSensorId == "4269") chart.Series.Add(new Series() { name = previousYear.ToString(), data = dataSetForSelectedPeriod });
                            if (iSensorId == "4267") chart.Series.Add(new Series() { name = previousYear.ToString(), data = dataSetForSelectedPeriod });
                            if (iSensorId == "4268") chart.Series.Add(new Series() { name = previousYear.ToString(), data = dataSetForSelectedPeriod });
                            if (iSensorId == "1659") chart.Series.Add(new Series() { name = previousYear.ToString(), data = dataSetForSelectedPeriod });
                            if (iSensorId == "586") chart.Series.Add(new Series() { name = previousYear.ToString(), data = dataSetForSelectedPeriod });
                            if (iSensorId == "1604") chart.Series.Add(new Series() { name = previousYear.ToString(), data = dataSetForSelectedPeriod });
                        }
                        chart.ColumnNameList = monthList;
                    }
                }

                // Previous Date Range
                if ((dsSensorHistoryLastPeriod != null) && (dsSensorHistoryLastPeriod.Tables.Count != 0) && isCompare)
                {
                    if (dsSensorHistoryLastPeriod.Tables[0].Rows.Count != 0)
                    {
                        int iCol = 0;
                        var previousYear = 2016;
                        var dataSetForPreviousPeriod = new List<double>();
                        var monthList = new List<string>();

                        if (dsSensorHistoryLastPeriod.Tables[0].Rows.Count > 60 && iSensorId != "586")
                        {
                            var ml = (from DataRow dr in dsSensorHistoryLastPeriod.Tables[0].DefaultView.Table.Rows
                                      select (DateTime)dr[0]).ToList();
                            monthList = ml.Select(x => new DateTime(x.Year, x.Month, 1).ToString()).Distinct().ToList();

                            var gruopData = from b in dsSensorHistoryLastPeriod.Tables[0].DefaultView.Table.AsEnumerable()
                                            group b by ((DateTime)b[0]).Month
                                            into g
                                            select g.Sum(x => x.Field<double>("Avg"));
                            foreach (var item in gruopData)
                            {
                                var calItem = 0.00;
                                calItem = (int)(item / 1000);
                                dataSetForPreviousPeriod.Add(Math.Round(calItem));
                            }

                        }
                        else
                        {
                            for (int i = 0; i < dsSensorHistoryLastPeriod.Tables[0].Rows.Count; i++)
                            {
                                double tbl0_V = 0.0;
                                var dtX = Convert.ToDateTime(dsSensorHistoryLastPeriod.Tables[0].Rows[i]["StartDT"]
                                    .ToString());
                                previousYear = dtX.Year;
                                tbl0_V = Convert.ToDouble(dsSensorHistoryLastPeriod.Tables["tbl0"].Rows[i]["Avg"]
                                    .ToString());
                                if (iSensorId != "586")
                                {
                                    tbl0_V = tbl0_V / 1000;
                                }

                                tbl0_V = Math.Round(tbl0_V);
                                if (tbl0_V < 0)
                                {
                                    tbl0_V = 0;
                                }

                                dataSetForPreviousPeriod.Add(tbl0_V);
                                monthList.Add(dtX.ToString("G"));
                                iCol++;
                            }
                        }
                        if (cookieCulture != null && cookieCulture.Value == "en-us")
                        {
                            if (iSensorId == "4269") chart.Series.Add(new Series() { name = previousYear.ToString(), data = dataSetForPreviousPeriod });
                            if (iSensorId == "4267") chart.Series.Add(new Series() { name = previousYear.ToString(), data = dataSetForPreviousPeriod });
                            if (iSensorId == "4268") chart.Series.Add(new Series() { name = previousYear.ToString(), data = dataSetForPreviousPeriod });
                            if (iSensorId == "1659") chart.Series.Add(new Series() { name = previousYear.ToString(), data = dataSetForPreviousPeriod });
                            if (iSensorId == "586") chart.Series.Add(new Series() { name = previousYear.ToString(), data = dataSetForPreviousPeriod });
                            if (iSensorId == "1604") chart.Series.Add(new Series() { name = previousYear.ToString(), data = dataSetForPreviousPeriod });

                        }
                        else
                        {
                            if (iSensorId == "4269") chart.Series.Add(new Series() { name = previousYear.ToString(), data = dataSetForPreviousPeriod });
                            if (iSensorId == "4267") chart.Series.Add(new Series() { name = previousYear.ToString(), data = dataSetForPreviousPeriod });
                            if (iSensorId == "4268") chart.Series.Add(new Series() { name = previousYear.ToString(), data = dataSetForPreviousPeriod });
                            if (iSensorId == "1659") chart.Series.Add(new Series() { name = previousYear.ToString(), data = dataSetForPreviousPeriod });
                            if (iSensorId == "586") chart.Series.Add(new Series() { name = previousYear.ToString(), data = dataSetForPreviousPeriod });
                            if (iSensorId == "1604") chart.Series.Add(new Series() { name = previousYear.ToString(), data = dataSetForPreviousPeriod });
                        }
                        chart.ColumnNameList = monthList;

                    }
                }
            }
            catch (Exception ex)
            {
                return Json(new { status = false }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { status = true, data = chart }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SetCulture(string culture)
        {
            // Validate input
            culture = CultureHelper.GetImplementedCulture(culture);
            // Save culture in a cookie
            HttpCookie cookie = Request.Cookies["_culture"];
            if (cookie != null)
                cookie.Value = culture;   // update cookie value
            else
            {
                cookie = new HttpCookie("_culture");
                cookie.Value = culture;
                cookie.Expires = DateTime.Now.AddYears(1);
            }
            Response.Cookies.Add(cookie);
            return RedirectToAction("Index");
        }

        public void SaveToPdf(string sDate, string eDate, bool isEnglish)
        {
            try
            {
                if (string.IsNullOrEmpty(sDate) || string.IsNullOrEmpty(eDate))
                {
                    var cday = DateTime.Today;
                    sDate = cday.AddMonths(-2).ToString();
                    eDate = cday.ToString();
                }
                int endUserId = Convert.ToInt32(Session["EndUserID"]);
                DateTime fDate = DateTime.Parse(sDate);
                DateTime tDate = DateTime.Parse(eDate);
                tDate = tDate.AddDays(2);
                fDate = fDate.AddDays(1);
                var electricityValues = new List<KeyValuePair<string, string>>();
                var hotWaterValues = new List<KeyValuePair<string, string>>();
                var coldWaterValues = new List<KeyValuePair<string, string>>();
                var heatValues = new List<KeyValuePair<string, string>>();
                var floorHeatValues = new List<KeyValuePair<string, string>>();
                var temperatureValues = new List<KeyValuePair<string, string>>();

                fDate = Convert.ToDateTime(fDate.Year + "-" + fDate.Month + "-" + fDate.Day);
                tDate = new DateTime(tDate.AddDays(-1).Year, tDate.AddDays(-1).Month, tDate.AddDays(-1).Day, 23, 59,
                    59);
                Session["dtFrom"] = fDate.ToString();
                Session["dtTo"] = tDate.ToString();
                var userName = Session["UserName"].ToString();
                var passkey = Session["PassKey"].ToString();
                var electricityTypId = 1;
                var hotColdWaterTypId = 5;

                //Get Electricity data for pdf
                DataSet dsSensorElectricityHistory;
                DataSet dsSensorColdWaterHistory;
                DataSet dsSensorHotWaterHistory;
                DataSet dsSensorHeatHistory;
                DataSet dsSensorFloorHeatHistory;
                DataSet dsSensorTemperatureHistory;
                DataSet gruopSensorData;
                using (var endUserService = new EndUserSoapClient())
                {
                    var strIdentifier = endUserService.Login(userName, passkey);
                    if (strIdentifier != "-5" || strIdentifier != "-7")
                    {
                        // Login ok
                        Session["StrID"] = strIdentifier;
                        Session["UserName"] = userName;
                        Session["PassKey"] = passkey;

                        Session["EndUserID"] = endUserService.GetEndUserID(strIdentifier);
                    }
                    gruopSensorData = endUserService.GetSensorValues(Session["strID"].ToString(), endUserId.ToString());

                    dsSensorElectricityHistory = endUserService.GetSensorHistoryPeriod(Session["strID"].ToString(), "9",
                        "1440", endUserId.ToString(), "4269", fDate.ToString(), tDate.ToString());
                    
                    dsSensorColdWaterHistory = endUserService.GetSensorHistoryPeriod(Session["strID"].ToString(), "9",
                        "1440", endUserId.ToString(), "4267", fDate.ToString(), tDate.ToString());
                    
                    dsSensorHotWaterHistory = endUserService.GetSensorHistoryPeriod(Session["strID"].ToString(), "9",
                        "1440", endUserId.ToString(), "4268", fDate.ToString(), tDate.ToString());
                    
                    dsSensorHeatHistory = endUserService.GetSensorHistoryPeriod(Session["strID"].ToString(), "9",
                        "1440", endUserId.ToString(), "1659", fDate.ToString(), tDate.ToString());
                   
                    dsSensorFloorHeatHistory = endUserService.GetSensorHistoryPeriod(Session["strID"].ToString(), "9",
                        "1440", endUserId.ToString(), "1604", fDate.ToString(), tDate.ToString());
                    
                    dsSensorTemperatureHistory = endUserService.GetSensorHistoryPeriod(Session["strID"].ToString(), "9",
                        "1440", endUserId.ToString(), "586", fDate.ToString(), tDate.ToString());
                    
                }
                PdfDataGenerator.GetReportData(fDate, tDate, endUserId, electricityValues, dsSensorElectricityHistory,
                    "4269");
                PdfDataGenerator.GetReportData(fDate, tDate, endUserId, coldWaterValues, dsSensorColdWaterHistory,
                    "4267");
                PdfDataGenerator.GetReportData(fDate, tDate, endUserId, hotWaterValues, dsSensorHotWaterHistory,
                    "4268");
                PdfDataGenerator.GetReportData(fDate, tDate, endUserId, heatValues, dsSensorHeatHistory, "1659");
                PdfDataGenerator.GetReportData(fDate, tDate, endUserId, floorHeatValues, dsSensorFloorHeatHistory,
                    "1604");
                PdfDataGenerator.GetReportData(fDate, tDate, endUserId, temperatureValues, dsSensorTemperatureHistory,
                    "586");

                PdfEvents e = new PdfEvents();
                e.StartDate = fDate;
                e.EndDate = tDate;
                e.IsEnglish = isEnglish;
                MemoryStream workStream = new MemoryStream();
                Document myDocument = new Document(PageSize.A4, 36, 36, 36 + 100, 80);

                var pw = PdfWriter.GetInstance(myDocument, workStream);
                pw.PageEvent = e;
                pw.CloseStream = false;
                myDocument.Open();
                var sensorGroupList = (from DataRow dr in gruopSensorData.Tables[0].DefaultView.Table.Rows
                    select new
                    {
                        SensorGroup = dr[0],
                        SensorGroupId = dr[1],
                        SensorId = dr[8],
                    }).ToList().OrderBy(x => x.SensorGroup);

                var sensorList = sensorGroupList
                    .Select(
                        item => new KeyValuePair<int, string>((int) item.SensorGroupId, item.SensorGroup.ToString()))
                    .ToList();

                //Create PDF
                PdfDataGenerator.CreatePdf(myDocument, fDate, tDate, electricityValues, hotWaterValues, coldWaterValues,
                    heatValues, floorHeatValues, temperatureValues, isEnglish, sensorList);

                myDocument.Close();
                byte[] byteInfo = workStream.ToArray();
                workStream.Write(byteInfo, 0, byteInfo.Length);
                workStream.Position = 0;
                Response.Buffer = true;
                Response.AddHeader("Content-Disposition",
                    "attachment; filename= " + Server.HtmlEncode("El_Online_" + DateTime.Now.ToString("d") + ".pdf"));
                Response.ContentType = "APPLICATION/pdf";
                Response.BinaryWrite(byteInfo);
            }
            catch (Exception ex)
            {
                Response.Redirect(@"~/");
            }
        }

        public JsonResult GetElectricityStatisticsByDateRange(string sDate, string eDate, bool isCompare)
        {
            int TypID = 0;
            if (sDate == null || eDate == null)
            {
                var cday = DateTime.Today;
                sDate = cday.AddYears(-1).ToString();
                eDate = cday.ToString();
            }
            var cookieCulture = Request.Cookies["_culture"];
            int endUserId = Convert.ToInt32(Session["EndUserID"]);
            DateTime fDate = DateTime.Parse(sDate);
            DateTime tDate = DateTime.Parse(eDate);
            ChartModel chart = new ChartModel();
            var lastYearFdate = fDate.AddYears(-1);
            var lastYearTdate = tDate.AddYears(-1);

            try
            {
                fDate = Convert.ToDateTime(fDate.Year + "-" + fDate.Month + "-" + fDate.Day);
                tDate = new DateTime(tDate.AddDays(-1).Year, tDate.AddDays(-1).Month, tDate.AddDays(-1).Day, 23, 59, 59);
                Session["dtFrom"] = fDate.ToString();
                Session["dtTo"] = tDate.ToString();
                //TypID = 1;

                DataSet dsSensorHistory;
                DataSet dsSensorHistoryLastPeriod = null;
                using (var endUserService = new EndUserSoapClient())
                {
                    dsSensorHistory = endUserService.GetSensorHistoryPeriod(Session["strID"].ToString(), "9", "1440", endUserId.ToString(), "4269", fDate.ToString(), tDate.ToString());
                    if (isCompare)
                    {
                        dsSensorHistoryLastPeriod = endUserService.GetSensorHistoryPeriod(Session["strID"].ToString(), "9", "1440", endUserId.ToString(), "4269", lastYearFdate.ToString(), lastYearTdate.ToString());
                    }
                }

                // Selected Date Range
                if ((dsSensorHistory != null) && (dsSensorHistory.Tables.Count != 0))
                {
                    if (dsSensorHistory.Tables[0].Rows.Count != 0)
                    {
                        int iCol = 0;
                        var dataSetForSelectedPeriod = new List<double>();
                        var monthList = new List<string>();
                        for (int i = 0; i < dsSensorHistory.Tables[0].Rows.Count; i++)
                        {
                            double tbl0_V = 0.0;

                            DateTime dtX;

                            dtX = Convert.ToDateTime(dsSensorHistory.Tables[0].Rows[i]["StartDT"].ToString());

                            tbl0_V = Convert.ToDouble(dsSensorHistory.Tables[0].Rows[i]["Avg"].ToString());
                            if (tbl0_V < 0)
                            {
                                tbl0_V = 0;
                            }

                            dataSetForSelectedPeriod.Add(tbl0_V);
                            monthList.Add(new DateTime(dtX.Year, dtX.Month, dtX.Day).ToString());
                            iCol++;
                        }
                        if (cookieCulture != null && cookieCulture.Value == "en-us")
                        {
                            chart.Series.Add(new Series() { name = "Electricity", data = dataSetForSelectedPeriod });
                        }
                        else
                        {
                            chart.Series.Add(new Series() { name = "Elförbrukning", data = dataSetForSelectedPeriod });
                        }
                        chart.ColumnNameList = monthList;
                    }
                }

                // Previous Date Range
                if ((dsSensorHistoryLastPeriod != null) && (dsSensorHistoryLastPeriod.Tables.Count != 0) && isCompare)
                {
                    if (dsSensorHistoryLastPeriod.Tables[0].Rows.Count != 0)
                    {
                        int iCol = 0;
                        var dataSetForPreviousPeriod = new List<double>();
                        for (int i = 0; i < dsSensorHistoryLastPeriod.Tables[0].Rows.Count; i++)
                        {
                            double tbl0_V = 0.0;

                            var dtX = Convert.ToDateTime(dsSensorHistory.Tables[0].Rows[i]["StartDT"].ToString());
                            tbl0_V = Convert.ToDouble(dsSensorHistoryLastPeriod.Tables["tbl0"].Rows[i]["Avg"].ToString());

                            if (tbl0_V < 0)
                            {
                                tbl0_V = 0;
                            }

                            dataSetForPreviousPeriod.Add(tbl0_V);
                            iCol++;
                        }
                        if (cookieCulture != null && cookieCulture.Value == "en-us")
                        {
                            chart.Series.Add(new Series() { name = "Electricity Previous", data = dataSetForPreviousPeriod });
                        }
                        else
                        {
                            chart.Series.Add(new Series() { name = "Elförbrukning Tidigare", data = dataSetForPreviousPeriod });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
            return Json(new { data = chart }, JsonRequestBehavior.AllowGet);
        }

    }
}
