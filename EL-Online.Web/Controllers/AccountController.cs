﻿using EL_Online.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EL_Online.Web.Controllers
{
    public class AccountController : Controller
    {
        // GET: Account
        public ActionResult Login()
        {
            Session["StrID"] = null;
            ViewBag.InvalidCredentials = "0";
            return View();
        }


        // POST: Account/Create
        [HttpPost]
        public ActionResult Login(Login model)
        {
            ViewBag.InvalidCredentials = "0";
            var endUserService = new EL_OnlineSeviceEndUser.EndUserSoapClient();
            var strIdentifier = endUserService.Login(model.UserName, model.Password);
            if (strIdentifier != "-5" || strIdentifier != "-7")
            {
                // Login ok
                Session["StrID"] = strIdentifier;
                Session["UserName"] = model.UserName.ToString();
                Session["PassKey"] = model.Password.ToString();

                Session["EndUserID"] = endUserService.GetEndUserID(strIdentifier);
                return RedirectToAction("Index", "Dashboard", new { area = "" });
            }
            ViewBag.InvalidCredentials = "-5";
            return View();
        }

    }

}
