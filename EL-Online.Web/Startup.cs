﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(EL_Online.Web.Startup))]
namespace EL_Online.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
