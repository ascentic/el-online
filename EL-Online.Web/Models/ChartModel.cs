﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EL_Online.Web.Models
{
    public class ChartModel
    {
        public ChartModel()
        {
            Series = new List<Series>();
            ColumnNameList = new List<string>();
        }

        public List<Series> Series { get; set; }
        public List<string> ColumnNameList { get; set; }

    }

    public class Series
    {
        public string name { get; set; }
        public List<double> data { get; set; }

    }
}