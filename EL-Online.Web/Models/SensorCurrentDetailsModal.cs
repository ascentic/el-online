﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EL_Online.Web.Models
{
    public class SensorCurrentDetailsModal
    {
        public string TypeName { get; set; }
        public string Value { get; set; }   
        public string SensorId { get; set; }
        public string Price { get; set; }
        public string Measurement { get; set; } 
        public string UnitName { get; set; } 
        public bool IsElectricity { get; set; }     
        public bool IsHotWater { get; set; }    
        public bool IsColdWater { get; set; }   
        public bool IsTemp { get; set; }
        public bool IsHeat { get; set; }
        public bool IsFloorHeat { get; set; }
    }
}