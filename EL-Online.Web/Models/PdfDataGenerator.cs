﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using EL_Online.Web.EL_OnlineSeviceEndUser;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;

namespace EL_Online.Web.Models
{
    public class PdfDataGenerator
    {
        public static void GetReportData(DateTime fDate, DateTime tDate, int endUserId, List<KeyValuePair<string, string>> monthElectricityValue, DataSet dsSensorHistory, string iSensorId)
        {
            try
            {
                // Rita
                if ((dsSensorHistory != null) && (dsSensorHistory.Tables.Count != 0))
                {
                    if (dsSensorHistory.Tables[0].Rows.Count != 0)
                    {
                        int iCol = 0;
                        for (int i = 0; i < dsSensorHistory.Tables[0].Rows.Count; i++)
                        {
                            double tbl0_V = 0.0;

                            DateTime dtX;

                            dtX = Convert.ToDateTime(dsSensorHistory.Tables[0].Rows[i]["StartDT"].ToString());
                            tbl0_V = Convert.ToDouble(dsSensorHistory.Tables[0].Rows[i]["Avg"].ToString());
                            if (iSensorId != "586")
                            { tbl0_V = tbl0_V / 1000; }

                            tbl0_V = Math.Round(tbl0_V);
                            if (tbl0_V < 0)
                            {
                                tbl0_V = 0;
                            }

                            iCol++;
                            monthElectricityValue.Add(new KeyValuePair<string, string>(dtX.ToString("d"), tbl0_V.ToString()));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
            }
        }

        public static void CreatePdf(Document myDocument, DateTime fDate, DateTime tDate, List<KeyValuePair<string, string>> electricityValues, List<KeyValuePair<string, string>> hotWaterValues, List<KeyValuePair<string, string>> coldWaterValues, List<KeyValuePair<string, string>> heatValues, List<KeyValuePair<string, string>> floorHeatValues, List<KeyValuePair<string, string>> temperatureValues, bool isEnglish, List<KeyValuePair<int, string>> sensorGroupList)
        {
            PdfPTable tableElectricity = new PdfPTable(2);
            tableElectricity.HorizontalAlignment = Element.ALIGN_CENTER;
            Font fontH = new Font(Font.FontFamily.COURIER, 14f, Font.BOLD, BaseColor.BLACK);
            var cellTl = new PdfPCell(isEnglish ? new Phrase("Date", fontH) : new Phrase("Datum", fontH));
            cellTl.HorizontalAlignment = Element.ALIGN_CENTER;
            cellTl.VerticalAlignment = Element.ALIGN_MIDDLE;
            cellTl.BorderColor = BaseColor.BLACK;
            var cellTr = new PdfPCell(new Phrase(sensorGroupList.Where(x => x.Key == 100).Select(z => z.Value).FirstOrDefault() + "(kWh)", fontH));
            tableElectricity.HeaderRows = 1;
            cellTr.HorizontalAlignment = Element.ALIGN_CENTER;
            cellTr.VerticalAlignment = Element.ALIGN_MIDDLE;
            cellTr.BorderColor = BaseColor.BLACK;
            tableElectricity.AddCell(cellTl);
            tableElectricity.AddCell(cellTr);
            foreach (var item in electricityValues)
            {
                Font font = new Font(Font.FontFamily.COURIER, 11f, Font.NORMAL, BaseColor.BLACK);
                var cellL = new PdfPCell(new Phrase(item.Key, font));
                var cellR = new PdfPCell(new Phrase(item.Value, font));
                cellL.HorizontalAlignment = Element.ALIGN_CENTER;
                cellL.VerticalAlignment = Element.ALIGN_MIDDLE;
                cellL.BorderColor = BaseColor.BLACK;
                cellR.HorizontalAlignment = Element.ALIGN_CENTER;
                cellR.VerticalAlignment = Element.ALIGN_MIDDLE;
                cellR.BorderColor = BaseColor.BLACK;
                tableElectricity.AddCell(cellL);
                tableElectricity.AddCell(cellR);
            }
            myDocument.Add(tableElectricity);
            myDocument.NewPage();

            myDocument.Add(new Paragraph("\n"));
            PdfPTable tableHotWater = new PdfPTable(2);
            tableHotWater.HorizontalAlignment = Element.ALIGN_CENTER;
            var hotWatercellTl = new PdfPCell(isEnglish ? new Phrase("Date", fontH) : new Phrase("Datum", fontH));
            hotWatercellTl.HorizontalAlignment = Element.ALIGN_CENTER;
            hotWatercellTl.VerticalAlignment = Element.ALIGN_MIDDLE;
            hotWatercellTl.BorderColor = BaseColor.BLACK;
            var hotWatercellTr = new PdfPCell(new Phrase(sensorGroupList.Where(x => x.Key == 120).Select(z => z.Value).FirstOrDefault() + "(liter)", fontH));
            hotWatercellTr.HorizontalAlignment = Element.ALIGN_CENTER;
            hotWatercellTr.VerticalAlignment = Element.ALIGN_MIDDLE;
            hotWatercellTr.BorderColor = BaseColor.BLACK;
            tableHotWater.HeaderRows = 1;
            tableHotWater.AddCell(hotWatercellTl);
            tableHotWater.AddCell(hotWatercellTr);
            foreach (var item in hotWaterValues)
            {
                Font font = new Font(Font.FontFamily.COURIER, 11f, Font.NORMAL, BaseColor.BLACK);
                var cellL = new PdfPCell(new Phrase(item.Key, font));
                var cellR = new PdfPCell(new Phrase(item.Value, font));
                cellL.HorizontalAlignment = Element.ALIGN_CENTER;
                cellL.VerticalAlignment = Element.ALIGN_MIDDLE;
                cellL.BorderColor = BaseColor.BLACK;
                cellR.HorizontalAlignment = Element.ALIGN_CENTER;
                cellR.VerticalAlignment = Element.ALIGN_MIDDLE;
                cellR.BorderColor = BaseColor.BLACK;
                tableHotWater.AddCell(cellL);
                tableHotWater.AddCell(cellR);
            }
            myDocument.Add(tableHotWater);
            myDocument.NewPage();

            myDocument.Add(new Paragraph("\n"));
            PdfPTable tableColdWater = new PdfPTable(2);
            tableColdWater.HorizontalAlignment = Element.ALIGN_CENTER;
            var coldWatercellTl = new PdfPCell(isEnglish ? new Phrase("Date", fontH) : new Phrase("Datum", fontH));
            coldWatercellTl.HorizontalAlignment = Element.ALIGN_CENTER;
            coldWatercellTl.VerticalAlignment = Element.ALIGN_MIDDLE;
            coldWatercellTl.BorderColor = BaseColor.BLACK;
            var coldWatercellTr = new PdfPCell(new Phrase(sensorGroupList.Where(x => x.Key == 110).Select(z => z.Value).FirstOrDefault() + "(liter)", fontH));
            coldWatercellTr.HorizontalAlignment = Element.ALIGN_CENTER;
            coldWatercellTr.VerticalAlignment = Element.ALIGN_MIDDLE;
            coldWatercellTr.BorderColor = BaseColor.BLACK;
            tableColdWater.HeaderRows = 1;
            tableColdWater.AddCell(coldWatercellTl);
            tableColdWater.AddCell(coldWatercellTr);
            foreach (var item in coldWaterValues)
            {
                Font font = new Font(Font.FontFamily.COURIER, 11f, Font.NORMAL, BaseColor.BLACK);
                var cellL = new PdfPCell(new Phrase(item.Key, font));
                var cellR = new PdfPCell(new Phrase(item.Value, font));
                cellL.HorizontalAlignment = Element.ALIGN_CENTER;
                cellL.VerticalAlignment = Element.ALIGN_MIDDLE;
                cellL.BorderColor = BaseColor.BLACK;
                cellR.HorizontalAlignment = Element.ALIGN_CENTER;
                cellR.VerticalAlignment = Element.ALIGN_MIDDLE;
                cellR.BorderColor = BaseColor.BLACK;
                tableColdWater.AddCell(cellL);
                tableColdWater.AddCell(cellR);
            }
            myDocument.Add(tableColdWater);
            myDocument.NewPage();

            myDocument.Add(new Paragraph("\n"));
            PdfPTable tableHeat = new PdfPTable(2);
            tableHeat.HorizontalAlignment = Element.ALIGN_CENTER;
            var heatcellTl = new PdfPCell(isEnglish ? new Phrase("Date", fontH) : new Phrase("Datum", fontH));
            heatcellTl.HorizontalAlignment = Element.ALIGN_CENTER;
            heatcellTl.VerticalAlignment = Element.ALIGN_MIDDLE;
            heatcellTl.BorderColor = BaseColor.BLACK;
            var heatcellTr = new PdfPCell(new Phrase(sensorGroupList.Where(x => x.Key == 130).Select(z => z.Value).FirstOrDefault() + "(kWh)", fontH));
            heatcellTr.HorizontalAlignment = Element.ALIGN_CENTER;
            heatcellTr.VerticalAlignment = Element.ALIGN_MIDDLE;
            heatcellTr.BorderColor = BaseColor.BLACK;
            tableHeat.HeaderRows = 1;
            tableHeat.AddCell(heatcellTl);
            tableHeat.AddCell(heatcellTr);
            foreach (var item in heatValues)
            {
                Font font = new Font(Font.FontFamily.COURIER, 11f, Font.NORMAL, BaseColor.BLACK);
                var cellL = new PdfPCell(new Phrase(item.Key, font));
                var cellR = new PdfPCell(new Phrase(item.Value, font));
                cellL.HorizontalAlignment = Element.ALIGN_CENTER;
                cellL.VerticalAlignment = Element.ALIGN_MIDDLE;
                cellL.BorderColor = BaseColor.BLACK;
                cellR.HorizontalAlignment = Element.ALIGN_CENTER;
                cellR.VerticalAlignment = Element.ALIGN_MIDDLE;
                cellR.BorderColor = BaseColor.BLACK;
                tableHeat.AddCell(cellL);
                tableHeat.AddCell(cellR);
            }
            myDocument.Add(tableHeat);
            myDocument.NewPage();

            myDocument.Add(new Paragraph("\n"));
            PdfPTable tableFloorHeat = new PdfPTable(2);
            tableFloorHeat.HorizontalAlignment = Element.ALIGN_CENTER;
            var floorHeatcellTl = new PdfPCell(isEnglish ? new Phrase("Date", fontH) : new Phrase("Datum", fontH));
            floorHeatcellTl.HorizontalAlignment = Element.ALIGN_CENTER;
            floorHeatcellTl.VerticalAlignment = Element.ALIGN_MIDDLE;
            floorHeatcellTl.BorderColor = BaseColor.BLACK;
            var floorHeatcellTr = new PdfPCell(new Phrase(sensorGroupList.Where(x => x.Key == 105).Select(z => z.Value).FirstOrDefault() + "(kWh)", fontH));
            floorHeatcellTr.HorizontalAlignment = Element.ALIGN_CENTER;
            floorHeatcellTr.VerticalAlignment = Element.ALIGN_MIDDLE;
            floorHeatcellTr.BorderColor = BaseColor.BLACK;
            tableFloorHeat.HeaderRows = 1;
            tableFloorHeat.AddCell(floorHeatcellTl);
            tableFloorHeat.AddCell(floorHeatcellTr);
            foreach (var item in floorHeatValues)
            {
                Font font = new Font(Font.FontFamily.COURIER, 11f, Font.NORMAL, BaseColor.BLACK);
                var cellL = new PdfPCell(new Phrase(item.Key, font));
                var cellR = new PdfPCell(new Phrase(item.Value, font));
                cellL.HorizontalAlignment = Element.ALIGN_CENTER;
                cellL.VerticalAlignment = Element.ALIGN_MIDDLE;
                cellL.BorderColor = BaseColor.BLACK;
                cellR.HorizontalAlignment = Element.ALIGN_CENTER;
                cellR.VerticalAlignment = Element.ALIGN_MIDDLE;
                cellR.BorderColor = BaseColor.BLACK;
                tableFloorHeat.AddCell(cellL);
                tableFloorHeat.AddCell(cellR);
            }
            myDocument.Add(tableFloorHeat);
            myDocument.NewPage();

            myDocument.Add(new Paragraph("\n"));
            PdfPTable tableTemperature = new PdfPTable(2);
            tableTemperature.HorizontalAlignment = Element.ALIGN_CENTER;
            var temperaturecellTl = new PdfPCell(isEnglish ? new Phrase("Date", fontH) : new Phrase("Datum", fontH));
            temperaturecellTl.HorizontalAlignment = Element.ALIGN_CENTER;
            temperaturecellTl.VerticalAlignment = Element.ALIGN_MIDDLE;
            temperaturecellTl.BorderColor = BaseColor.BLACK;
            var temperaturecellTr = new PdfPCell(new Phrase(sensorGroupList.Where(x => x.Key == 140).Select(z => z.Value).FirstOrDefault() + "( °C )", fontH));
            temperaturecellTr.HorizontalAlignment = Element.ALIGN_CENTER;
            temperaturecellTr.VerticalAlignment = Element.ALIGN_MIDDLE;
            temperaturecellTr.BorderColor = BaseColor.BLACK;
            tableTemperature.HeaderRows = 1;
            tableTemperature.AddCell(temperaturecellTl);
            tableTemperature.AddCell(temperaturecellTr);
            foreach (var item in temperatureValues)
            {
                Font font = new Font(Font.FontFamily.COURIER, 11f, Font.NORMAL, BaseColor.BLACK);
                var cellL = new PdfPCell(new Phrase(item.Key, font));
                var cellR = new PdfPCell(new Phrase(item.Value, font));
                cellL.HorizontalAlignment = Element.ALIGN_CENTER;
                cellL.VerticalAlignment = Element.ALIGN_MIDDLE;
                cellL.BorderColor = BaseColor.BLACK;
                cellR.HorizontalAlignment = Element.ALIGN_CENTER;
                cellR.VerticalAlignment = Element.ALIGN_MIDDLE;
                cellR.BorderColor = BaseColor.BLACK;
                tableTemperature.AddCell(cellL);
                tableTemperature.AddCell(cellR);
            }
            myDocument.Add(tableTemperature);
        }
    }

    public class PdfEvents : PdfPageEventHelper
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool IsEnglish { get; set; }
        public override void OnEndPage(PdfWriter writer, Document document)
        {
            PdfPTable table = new PdfPTable(1);
            table.WidthPercentage = 100; //PdfPTable.writeselectedrows below didn't like this
            table.TotalWidth = document.PageSize.Width - document.LeftMargin - document.RightMargin; //this centers [table]
            PdfPTable table2 = new PdfPTable(2);
            table.DefaultCell.Border = Rectangle.NO_BORDER;
            table2.DefaultCell.Border = Rectangle.NO_BORDER;
            table2.WidthPercentage = 30;
            table2.HorizontalAlignment = Element.ALIGN_LEFT;
            //logo 
            PdfPCell cell2 = new PdfPCell(Image.GetInstance(HttpRuntime.AppDomainAppPath + @"Content\images\minImages\el_logo.png"));
            cell2.FixedHeight = 30f;
            cell2.Colspan = 2;
            table.HorizontalAlignment = 0;
            cell2.HorizontalAlignment = Element.ALIGN_CENTER;
            cell2.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell2.Border = Rectangle.NO_BORDER;
            table2.AddCell(cell2);

            //title
            var fontH = new Font(Font.FontFamily.COURIER, 16f, Font.BOLD, BaseColor.DARK_GRAY);
            cell2 = new PdfPCell(IsEnglish ? new Paragraph("EL-Online Summary Report", fontH) : new Paragraph("EL-Online Förbrukningsrapport", fontH));
            cell2.HorizontalAlignment = Element.ALIGN_CENTER;
            cell2.Colspan = 2;
            cell2.Border = Rectangle.NO_BORDER;
            table2.AddCell(cell2);

            //title
            cell2 = new PdfPCell(IsEnglish ? new Paragraph("Time Period" + " " + StartDate.ToString("d") + " to " + EndDate.ToString("d"))
                : new Paragraph("Tidsperiod" + " " + StartDate.ToString("d") + " till " + EndDate.ToString("d")));
            cell2.HorizontalAlignment = Element.ALIGN_CENTER;
            cell2.Colspan = 2;
            cell2.Border = Rectangle.NO_BORDER;
            table2.AddCell(cell2);

            PdfPCell cell = new PdfPCell(table2);
            cell.Border = Rectangle.NO_BORDER;
            table.AddCell(cell);

            PdfContentByte cb = writer.DirectContent;
            cb.SaveState();
            String text = writer.PageNumber.ToString();
            cb.BeginText();
            cb.SetFontAndSize(BaseFont.CreateFont(), 12);
            cb.MoveText(((document.PageSize.Width / 2) - 2), 40);
            cb.ShowText(text);
            cb.EndText();
            cb.RestoreState();

            table.WriteSelectedRows(0, -1, document.LeftMargin, document.PageSize.Height - 36, writer.DirectContent);
        }

    }
}

