﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EL_Online.Web.Models
{
    public class StartWaterSensorStatisticModal
    {
        public DateTime Date { get; set; }
        public double Value { get; set; }
        public string Month { get; set; }
    }
}