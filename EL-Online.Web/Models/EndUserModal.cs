﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EL_Online.Web.Models
{
    public abstract class EndUserModal
    {
        public string ZipCode { get; set; }
        public string SiteName { get; set; }
        public string Note { get; set; }
        public string City { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Debiteringkod { get; set; }
        public string Objektnamn { get; set; }
    }
}