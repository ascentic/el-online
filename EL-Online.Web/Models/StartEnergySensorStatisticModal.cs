﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EL_Online.Web.Models
{
    public class StartEnergySensorStatisticModal
    {
        public DateTime Date { get; set; }  
        public double SensorValue { get; set; } 
        public string Month { get; set; } 
    }
}