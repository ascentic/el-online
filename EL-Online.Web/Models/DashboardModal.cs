﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EL_Online.Web.Models
{
    public class DashboardModal : EndUserModal
    {
        public List<StartEnergySensorStatisticModal> StartEnergySensorStatisticList { get; set; } 
        public List<SensorCurrentDetailsModal> SensorCurrentDetailList { get; set; }    
        public List<StartWaterSensorStatisticModal> StartWaterSensorStatisticList { get; set; }
    }


}