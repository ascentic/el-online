
/* custom js */
var d = new Date();

var cYear = d.getFullYear();
var month = d.getMonth() + 1;
var day = d.getDate();


$('#monthDropdownElectricity option:eq(' + (month - 1) + ')').prop('selected', true);
$('#yearDropdownElectricity option[value="' + cYear + '"]').prop('selected', true);
$('#yearForMonthDropdownElectricity option[value="' + cYear + '"]').prop('selected', true);

$('#monthDropdownColdWater option:eq(' + (month - 1) + ')').prop('selected', true);
$('#yearDropdownColdWater option[value="' + cYear + '"]').prop('selected', true);
$('#yearForMonthDropdownColdWater option[value="' + cYear + '"]').prop('selected', true);

$('#monthDropdownHotWater option:eq(' + (month - 1) + ')').prop('selected', true);
$('#yearDropdownHotWater option[value="' + cYear + '"]').prop('selected', true);
$('#yearForMonthDropdownHotWater option[value="' + cYear + '"]').prop('selected', true);

$('#monthDropdownHeat option:eq(' + (month - 1) + ')').prop('selected', true);
$('#yearDropdownHeat option[value="' + cYear + '"]').prop('selected', true);
$('#yearForMonthDropdownHeat option[value="' + cYear + '"]').prop('selected', true);

$('#monthDropdownFloorHeat option:eq(' + (month - 1) + ')').prop('selected', true);
$('#yearDropdownFloorHeat option[value="' + cYear + '"]').prop('selected', true);
$('#yearForMonthDropdownFloorHeat option[value="' + cYear + '"]').prop('selected', true);

$('#monthDropdownTemp option:eq(' + (month - 1) + ')').prop('selected', true);
$('#yearDropdownTemp option[value="' + cYear + '"]').prop('selected', true);
$('#yearForMonthDropdownTemp option[value="' + cYear + '"]').prop('selected', true);

var months = ['Jan', 'Feb', 'Mar', 'Apr', 'Maj', 'Juni', 'Juli', 'Aug', 'Sep', 'Okt', 'Nov', 'Dec'];
var isEnglishText = $('#isEnglishText').val();
var isEnglish = true;
var lang = "en-us"
if (isEnglishText == "en-us") {
    months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    isEnglish = true;
    lang = "en-us"
} else if (isEnglishText == "sv") {
    months = ['Jan', 'Feb', 'Mar', 'Apr', 'Maj', 'Juni', 'Juli', 'Aug', 'Sep', 'Okt', 'Nov', 'Dec'];
    isEnglish = false;
    lang = "sv"
}

function printPdf(elem) {
    document.getElementById("pdfLink").href = "/Dashboard/SaveToPdf?sDate=" + $("#pdfStartDate-input").val() + "&eDate=" + $("#pdfEndDate-input").val() + "&isEnglish=" + isEnglish;
    $('#pdfLink')[0].click();
}

function showPrintSetting() {
    $("#printSettingPannel").toggle();
}

/* side nav - more about apartment panel */
function openNav() {
    document.getElementById("sideNav").style.width = "250px";
}

function closeNav() {
    document.getElementById("sideNav").style.width = "0";
}

/* accordion active class */
$('.panel-heading a').click(function () {
    $('.panel-heading').removeClass('active');
    $(this).parents('.panel-heading').addClass('active');

    $('.panel-title').removeClass('active');
    $(this).parent().addClass('active');
});

$(function () {

    $('#electricityStartDate').datetimepicker({ format: "YYYY-MM-DD", locale: lang });
    $('#electricityEndDate').datetimepicker({ format: "YYYY-MM-DD", locale: lang });
    $('#coldWaterStartDate').datetimepicker({ format: "YYYY-MM-DD", locale: lang });
    $('#coldWaterEndDate').datetimepicker({ format: "YYYY-MM-DD", locale: lang });
    $('#warmWaterStartDate').datetimepicker({ format: "YYYY-MM-DD", locale: lang });
    $('#warmWaterEndDate').datetimepicker({ format: "YYYY-MM-DD", locale: lang });
    $('#floorHeatStartDate').datetimepicker({ format: "YYYY-MM-DD", locale: lang });
    $('#floorHeatEndDate').datetimepicker({ format: "YYYY-MM-DD", locale: lang });
    $('#heatStartDate').datetimepicker({ format: "YYYY-MM-DD", locale: lang });
    $('#heatEndDate').datetimepicker({ format: "YYYY-MM-DD", locale: lang });
    $('#temperatureStartDate').datetimepicker({ format: "YYYY-MM-DD", locale: lang });
    $('#temperatureEndDate').datetimepicker({ format: "YYYY-MM-DD", locale: lang });
    $('#pdfStartDate').datetimepicker({ format: "YYYY-MM-DD", locale: lang });
    $('#pdfEndDate').datetimepicker({ format: "YYYY-MM-DD", locale: lang });

    $("#dateRangePannelElectricity").hide();
    $("#monthlyPannelElectricity").hide();
    $("#dateRangePannelColdWater").hide();
    $("#monthlyPannelColdWater").hide();
    $("#dateRangePannelWarmWater").hide();
    $("#monthlyPannelHotWater").hide();
    $("#dateRangePannelHeat").hide();
    $("#monthlyPannelHeat").hide();
    $("#dateRangePannelFloorHeat").hide();
    $("#monthlyPannelFloorHeat").hide();
    $("#dateRangePannelTemp").hide();
    $("#monthlyPannelTemp").hide();

    var lastWeek = new Date();
    lastWeek.setDate(lastWeek.getDate() - 7);
    lastWeek = new Date(lastWeek);
    var output1 = lastWeek.getFullYear() + '-' + (lastWeek.getMonth() < 10 ? '0' : '') + (lastWeek.getMonth() + 1) + '-' + (lastWeek.getDate() < 10 ? '0' : '') + lastWeek.getDate();
    var output2 = d.getFullYear() + '-' + (month < 10 ? '0' : '') + month + '-' + (day < 10 ? '0' : '') + day;

    $("#electricityStartDate-input").val(output1);
    $("#electricityEndDate-input").val(output2);
    $("#coldWaterStartDate-input").val(output1);
    $("#coldWaterEndDate-input").val(output2);
    $("#warmWaterStartDate-input").val(output1);
    $("#warmWaterEndDate-input").val(output2);
    $("#floorHeatStartDate-input").val(output1);
    $("#floorHeatEndDate-input").val(output2);
    $("#heatStartDate-input").val(output1);
    $("#heatEndDate-input").val(output2);
    $("#temperatureStartDate-input").val(output1);
    $("#temperatureEndDate-input").val(output2);

    var pdfEdate = d.getFullYear() + '-' + (month < 10 ? '0' : '') + month + '-' + (day < 10 ? '0' : '') + day;
    var pdfSdate = (d.getFullYear() - 1) + '-' + (month < 10 ? '0' : '') + month + '-' + (day < 10 ? '0' : '') + day;
    $("#pdfStartDate-input").val(pdfSdate);
    $("#pdfEndDate-input").val(pdfEdate);

});

/* electricity chart starts here */
$(function () {
    var isCompare = false;
    loadElectricityChart(isCompare);
    $("input[name='optradioElectricity']").change(function () {
        // Do something interesting here
        if ($("input[name='optradioElectricity']:checked").val() == "compare") {
            isCompare = true;
            $('#electricityRadioReset').show();
            loadElectricityChart(true);
        } else {
            $('#electricityRadioReset').hide();
            isCompare = false;
            loadElectricityChart(false);
        }

    });

    $("#monthDropdownElectricity").change(function () {
        var output1 = ($('#yearDropdownElectricity').val()) + '-' + $('#monthDropdownElectricity').val() + '-' + '01';
        var output2 = ($('#yearDropdownElectricity').val()) + '-' + (($('#monthDropdownElectricity').val() * 1) + 1) + '-' + '01';
        $("#electricityStartDate-input").val(output1);
        $("#electricityEndDate-input").val(output2);
        loadElectricityChart(isCompare);
    });

    $("#yearForMonthDropdownElectricity").change(function () {
        var output1 = ($('#yearForMonthDropdownElectricity').val()) + '-' + $('#monthDropdownElectricity').val() + '-' + '01';
        var output2 = ($('#yearForMonthDropdownElectricity').val()) + '-' + (($('#monthDropdownElectricity').val() * 1) + 1) + '-' + '01';
        $("#electricityStartDate-input").val(output1);
        $("#electricityEndDate-input").val(output2);
        loadElectricityChart(isCompare);
    });

    $("#yearDropdownElectricity").change(function () {
        var output1 = ($('#yearDropdownElectricity').val()) + '-' + $('#monthDropdownElectricity').val() + '-' + '01';
        var output2 = (($('#yearDropdownElectricity').val() * 1) + 1) + '-' + $('#monthDropdownElectricity').val() + '-' + '01';
        $("#electricityStartDate-input").val(output1);
        $("#electricityEndDate-input").val(output2);
        loadElectricityChart(isCompare);
    });

    $('#electricityShowResultButton').click(function () {
        if ($("#electricityStartDate-input").val() != null ||
            $("#electricityEndDate-input").val() != null) {
            loadElectricityChart(false);
        }
    });

    $("#timePeriodDropdownElectricity").change(function () {

        var ddVal = $('#timePeriodDropdownElectricity').val();
        switch (ddVal) {
            case "lw":
                $("#monthlyPannelElectricity").hide();
                $("#electricityCompairPanel").show();
                $("#dateRangePannelElectricity").hide();
                var lastWeek = new Date();
                lastWeek.setDate(lastWeek.getDate() - 7);
                lastWeek = new Date(lastWeek);
                var output1Ld = lastWeek.getFullYear() + '-' + (lastWeek.getMonth() < 10 ? '0' : '') + (lastWeek.getMonth() + 1) + '-' + (lastWeek.getDate() < 10 ? '0' : '') + lastWeek.getDate();
                var output2Ld = d.getFullYear() + '-' + (month < 10 ? '0' : '') + month + '-' + (day < 10 ? '0' : '') + day;
                $("#electricityStartDate-input").val(output1Ld);
                $("#electricityEndDate-input").val(output2Ld);
                loadElectricityChart(isCompare);
                break;
            case "lm":
                $("#dateRangePannelElectricity").hide();
                $("#electricityCompairPanel").show();
                $("#monthlyPannelElectricity").show();
                $("#monthDropdownElectricity").show();
                $("#yearDropdownElectricity").hide();
                $("#yearForMonthDropdownElectricity").show();
                var output1Lm = ($('#yearForMonthDropdownElectricity').val()) + '-' + $('#monthDropdownElectricity').val() + '-' + '01';
                var output2Lm = ($('#yearForMonthDropdownElectricity').val()) + '-' + (($('#monthDropdownElectricity').val() * 1) + 1) + '-' + '01';
                $("#electricityStartDate-input").val(output1Lm);
                $("#electricityEndDate-input").val(output2Lm);
                loadElectricityChart(isCompare);
                break;
            case "ly":
                $("#dateRangePannelElectricity").hide();
                $("#electricityCompairPanel").show();
                $("#monthlyPannelElectricity").show();
                $("#monthDropdownElectricity").hide();
                $("#yearDropdownElectricity").show();
                $("#yearForMonthDropdownElectricity").hide();
                var output1Ly = ($('#yearDropdownElectricity').val()) + '-' + '01' + '-' + '01';
                var output2Ly = (($('#yearDropdownElectricity').val() * 1) + 1) + '-' + '01' + '-' + '01';
                $("#electricityStartDate-input").val(output1Ly);
                $("#electricityEndDate-input").val(output2Ly);
                loadElectricityChart(isCompare);
                break;

            default:
                $("#monthlyPannelElectricity").hide();
                $("#electricityCompairPanel").hide();
                $("#dateRangePannelElectricity").show();
                var output1Def = (d.getFullYear() - 1) + '-' + (month < 10 ? '0' : '') + month + '-' + (day < 10 ? '0' : '') + day;
                var output2Def = d.getFullYear() + '-' + (month < 10 ? '0' : '') + month + '-' + (day < 10 ? '0' : '') + day;
                $("#electricityStartDate-input").val(output1Def);
                $("#electricityEndDate-input").val(output2Def);
                loadElectricityChart(false);
                break;
        }
    });


    function loadElectricityChart(isCompare) {

        var sdate = $("#electricityStartDate-input").val();
        var edate = $("#electricityEndDate-input").val();

        $.ajax({
            type: "GET",
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            url: '/Dashboard/GetStatisticsByDateRange/',
            data: { sDate: sdate, eDate: edate, isCompare: isCompare, iSensorId: "4269" },
            success: function (data) {
                if (data.status == false) { window.location.href = '/Account/Login' }
                var seriesData = data.data.Series;
                var categories = data.data.ColumnNameList;

                if (seriesData.length == 0) {
                    $('#errorElectricity').show();
                    $('#container1').hide();
                } else {
                    $('#errorElectricity').hide();
                    $('#container1').show();
                }

                Highcharts.Color.prototype.parsers.push({
                    regex: /^[a-z]+$/,
                    parse: function (result) {
                        var rgb = new RGBColor(result[0]);
                        if (rgb.ok) {
                            return [rgb.r, rgb.g, rgb.b, 1]; // returns rgba to Highcharts
                        }
                    }
                });

                var chart = Highcharts.chart('container1',
                    {
                        chart: {
                            type: seriesData[0].data.length < 30 ? 'column' : 'line'
                        },
                        credits: {
                            enabled: false
                        },
                        colors: ['rgba(6,171,28,0.3)', '#2D3F70'],
                        title: {
                            text: "" //isEnglish ? 'Electricity Usage' : 'Elforbrukning'
                        },
                        yAxis: {
                            title: {
                                text: ' kWh '
                            },
                            labels: {
                                formatter: function () {
                                    return this.value;
                                }

                            }
                        },
                        xAxis: {
                            categories: categories,

                            labels: {
                                formatter: function () {
                                    var date;
                                    if (!isEnglish) {
                                        var arr = this.value.split(/[- :/]/)
                                        date = new Date(arr[0], arr[1] - 1, arr[2], arr[3], arr[4], arr[5]);
                                    } else {
                                        date = new Date(this.value);
                                    }

                                    if (date.getHours() == 0) {
                                        return months[date.getMonth()];
                                    } else {
                                        return months[date.getMonth()] + ' ' + date.getDate();
                                    }
                                }
                            }
                        },
                        series: seriesData,
                        tooltip: {
                            formatter: function () {
                                var date;
                                if (!isEnglish) {
                                    var arr = this.x.split(/[- :/]/)
                                    date = new Date(arr[0], arr[1] - 1, arr[2], arr[3], arr[4], arr[5]);
                                } else {
                                    date = new Date(this.x);
                                }

                                if (date.getHours() == 0) {
                                    return ' <b>' + this.series.name + '</b> - <b>' + months[date.getMonth()] + '</b> - <b>' + this.y + '</b> kWh ';
                                } else {
                                    return ' <b>' + this.series.name + '</b> - <b>' + months[date.getMonth()] + ' ' + date.getDate() + '</b> - <b>' + this.y + '</b> kWh ';
                                }
                            }
                        }

                    });

            }
        });
    }
});
/* electricity chart ends here */

/* cold water chart starts here */
$(function () {
    var isCompare = false;
    loadColdWaterChart(isCompare);
    $("input[name='optradioColdWater']").change(function () {
        // Do something interesting here
        if ($("input[name='optradioColdWater']:checked").val() == "compare") {
            $('#coldWaterRadioReset').show();
            isCompare = true;
            loadColdWaterChart(true);
        } else {
            $('#coldWaterRadioReset').hide();
            isCompare = false;
            loadColdWaterChart(false);
        }
    });

    $("#monthDropdownColdWater").change(function () {
        var output1 = ($('#yearDropdownColdWater').val()) + '-' + $('#monthDropdownColdWater').val() + '-' + '01';
        var output2 = ($('#yearDropdownColdWater').val()) + '-' + (($('#monthDropdownColdWater').val() * 1) + 1) + '-' + '01';
        $("#coldWaterStartDate-input").val(output1);
        $("#coldWaterEndDate-input").val(output2);
        loadColdWaterChart(isCompare);
    });

    $("#yearForMonthDropdownColdWater").change(function () {
        var output1 = ($('#yearForMonthDropdownColdWater').val()) + '-' + $('#monthDropdownColdWater').val() + '-' + '01';
        var output2 = ($('#yearForMonthDropdownColdWater').val()) + '-' + (($('#monthDropdownColdWater').val() * 1) + 1) + '-' + '01';
        $("#coldWaterStartDate-input").val(output1);
        $("#coldWaterEndDate-input").val(output2);
        loadColdWaterChart(isCompare);
    });

    $("#yearDropdownColdWater").change(function () {
        var output1 = ($('#yearDropdownColdWater').val()) + '-' + $('#monthDropdownColdWater').val() + '-' + '01';
        var output2 = (($('#yearDropdownColdWater').val() * 1) + 1) + '-' + $('#monthDropdownColdWater').val() + '-' + '01';
        $("#coldWaterStartDate-input").val(output1);
        $("#coldWaterEndDate-input").val(output2);
        loadColdWaterChart(isCompare);
    });

    $("#timePeriodDropdownColdWater").change(function () {

        var ddVal = $('#timePeriodDropdownColdWater').val();
        switch (ddVal) {
            case "lw":
                var lastWeek = new Date();
                lastWeek.setDate(lastWeek.getDate() - 7);
                lastWeek = new Date(lastWeek);
                var output1Ld = lastWeek.getFullYear() + '-' + (lastWeek.getMonth() < 10 ? '0' : '') + (lastWeek.getMonth() + 1) + '-' + (lastWeek.getDate() < 10 ? '0' : '') + lastWeek.getDate();
                var output2Ld = d.getFullYear() + '-' + (month < 10 ? '0' : '') + month + '-' + (day < 10 ? '0' : '') + day;
                $("#coldWaterStartDate-input").val(output1Ld);
                $("#coldWaterEndDate-input").val(output2Ld);
                $("#monthlyPannelColdWater").hide();
                $("#dateRangePannelColdWater").hide();
                loadColdWaterChart(isCompare);
                break;
            case "lm":
                $("#dateRangePannelColdWater").hide();
                $("#monthlyPannelColdWater").show();
                $("#monthDropdownColdWater").show();
                $("#yearDropdownColdWater").hide();
                $("#yearForMonthDropdownColdWater").show();
                var output1Lm = ($('#yearForMonthDropdownColdWater').val()) + '-' + $('#monthDropdownColdWater').val() + '-' + '01';
                var output2Lm = ($('#yearForMonthDropdownColdWater').val()) + '-' + (($('#monthDropdownColdWater').val() * 1) + 1) + '-' + '01';
                $("#coldWaterStartDate-input").val(output1Lm);
                $("#coldWaterEndDate-input").val(output2Lm);
                loadColdWaterChart(isCompare);
                break;
            case "ly":
                $("#dateRangePannelColdWater").hide();
                $("#monthlyPannelColdWater").show();
                $("#monthDropdownColdWater").hide();
                $("#yearDropdownColdWater").show();
                $("#yearForMonthDropdownColdWater").hide();
                var output1Ly = ($('#yearDropdownColdWater').val()) + '-' + '01' + '-' + '01';
                var output2Ly = (($('#yearDropdownColdWater').val() * 1) + 1) + '-' + '01' + '-' + '01';
                $("#coldWaterStartDate-input").val(output1Ly);
                $("#coldWaterEndDate-input").val(output2Ly);
                loadColdWaterChart(isCompare);
                break;
            default:
                $("#monthlyPannelColdWater").hide();
                $("#dateRangePannelColdWater").show();
                $("#coldWaterCompairPanel").hide();
                var output1Def = (d.getFullYear() - 1) + '-' + (month < 10 ? '0' : '') + month + '-' + (day < 10 ? '0' : '') + day;
                var output2Def = d.getFullYear() + '-' + (month < 10 ? '0' : '') + month + '-' + (day < 10 ? '0' : '') + day;
                $("#coldWaterStartDate-input").val(output1Def);
                $("#coldWaterEndDate-input").val(output2Def);
                loadColdWaterChart(false);
                break;
        }
    });

    $('#coldWaterShowResultButton').click(function () {
        if ($("#coldWaterStartDate-input").val() != null || $("#coldWaterEndDate-input").val() != null) {
            loadColdWaterChart(false);
        }
    });

    function loadColdWaterChart(isCompare) {

        var sdate = $("#coldWaterStartDate-input").val();
        var edate = $("#coldWaterEndDate-input").val();

        $.ajax({
            type: "GET",
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            url: '/Dashboard/GetStatisticsByDateRange/',
            data: { sDate: sdate, eDate: edate, isCompare: isCompare, iSensorId: "4267" },
            success: function (data) {
                if (data.status == false) { window.location.href = '/Account/Login' }
                var seriesDataColdWater = data.data.Series;
                var categories = data.data.ColumnNameList;

                if (seriesDataColdWater.length == 0) {
                    $('#errorColdWater').show();
                    $('#container2').hide();
                } else {
                    $('#errorColdWater').hide();
                    $('#container2').show();
                }

                var chart = Highcharts.chart('container2',
                    {
                        chart: {
                            type: seriesDataColdWater[0].data.length < 30 ? 'column' : 'line'
                        },
                        title: {
                            text: ""//isEnglish ? 'Cold Water Usage' : 'Kallvattenanvandning'
                        },
                        colors: ['rgba(29,195,255,0.3)', '#2D3F70'],
                        credits: {
                            enabled: false
                        },
                        yAxis: {
                            title: {
                                text: ' Liter '
                            },
                            labels: {
                                formatter: function () {
                                    return this.value;
                                }

                            }
                        },
                        xAxis: {
                            categories: categories,

                            labels: {
                                formatter: function () {
                                    var date;
                                    if (!isEnglish) {
                                        var arr = this.value.split(/[- :/]/)
                                        date = new Date(arr[0], arr[1] - 1, arr[2], arr[3], arr[4], arr[5]);
                                    } else {
                                        date = new Date(this.value);
                                    }

                                    if (date.getHours() == 0) {
                                        return months[date.getMonth()];
                                    } else {
                                        return months[date.getMonth()] + ' ' + date.getDate();
                                    }
                                }
                            }
                        },
                        series: seriesDataColdWater,
                        tooltip: {
                            formatter: function () {
                                var date;
                                if (!isEnglish) {
                                    var arr = this.x.split(/[- :/]/)
                                    date = new Date(arr[0], arr[1] - 1, arr[2], arr[3], arr[4], arr[5]);
                                } else {
                                    date = new Date(this.x);
                                }

                                if (date.getHours() == 0) {
                                    return ' <b>' + this.series.name + '</b> - <b>' + months[date.getMonth()] + '</b> - <b>' + this.y + '</b> Liter ';
                                } else {
                                    return ' <b>' + this.series.name + '</b> - <b>' + months[date.getMonth()] + ' ' + date.getDate() + '</b> - <b>' + this.y + '</b> Liter ';
                                }
                            }
                        }

                    });

            }
        });
    }

});
/* cold water chart ends here */

/* warm water chart starts here */
$(function () {
    var isCompare = false;
    loadHotWaterChart(isCompare);
    $("input[name='optradioHotWater']").change(function () {
        // Do something interesting here
        if ($("input[name='optradioHotWater']:checked").val() == "compare") {
            $('#hotWaterRadioReset').show();
            isCompare = true;
            loadHotWaterChart(true);
        } else {
            $('#hotWaterRadioReset').hide();
            isCompare = false;
            loadHotWaterChart(false);
        }
    });

    $("#monthDropdownHotWater").change(function () {
        var output1 = ($('#yearDropdownHotWater').val()) + '-' + $('#monthDropdownHotWater').val() + '-' + '01';
        var output2 = ($('#yearDropdownHotWater').val()) + '-' + (($('#monthDropdownHotWater').val() * 1) + 1) + '-' + '01';
        $("#warmWaterStartDate-input").val(output1);
        $("#warmWaterEndDate-input").val(output2);
        loadHotWaterChart(isCompare);
    });

    $("#yearForMonthDropdownHotWater").change(function () {
        var output1 = ($('#yearForMonthDropdownHotWater').val()) + '-' + $('#monthDropdownHotWater').val() + '-' + '01';
        var output2 = ($('#yearForMonthDropdownHotWater').val()) + '-' + (($('#monthDropdownHotWater').val() * 1) + 1) + '-' + '01';
        $("#warmWaterStartDate-input").val(output1);
        $("#warmWaterEndDate-input").val(output2);
        loadHotWaterChart(isCompare);
    });

    $("#yearDropdownHotWater").change(function () {
        var output1 = ($('#yearDropdownHotWater').val()) + '-' + $('#monthDropdownHotWater').val() + '-' + '01';
        var output2 = (($('#yearDropdownHotWater').val() * 1) + 1) + '-' + $('#monthDropdownHotWater').val() + '-' + '01';
        $("#warmWaterStartDate-input").val(output1);
        $("#warmWaterEndDate-input").val(output2);
        loadHotWaterChart(isCompare);
    });

    $("#timePeriodDropdownHotWater").change(function () {

        var ddVal = $('#timePeriodDropdownHotWater').val();
        switch (ddVal) {
            case "lw":
                var lastWeek = new Date();
                lastWeek.setDate(lastWeek.getDate() - 7);
                lastWeek = new Date(lastWeek);
                var output1Ld = lastWeek.getFullYear() + '-' + (lastWeek.getMonth() < 10 ? '0' : '') + (lastWeek.getMonth() + 1) + '-' + (lastWeek.getDate() < 10 ? '0' : '') + lastWeek.getDate();
                var output2Ld = d.getFullYear() + '-' + (month < 10 ? '0' : '') + month + '-' + (day < 10 ? '0' : '') + day;
                $("#monthlyPannelHotWater").hide();
                $("#dateRangePannelWarmWater").hide();
                $("#warmWaterStartDate-input").val(output1Ld);
                $("#warmWaterEndDate-input").val(output2Ld);
                loadHotWaterChart(isCompare);
                break;
            case "lm":
                $("#dateRangePannelWarmWater").hide();
                $("#monthlyPannelHotWater").show();
                $("#monthDropdownHotWater").show();
                $("#yearDropdownHotWater").hide();
                $("#yearForMonthDropdownHotWater").show();
                var output1Lm = ($('#yearForMonthDropdownHotWater').val()) + '-' + $('#monthDropdownHotWater').val() + '-' + '01';
                var output2Lm = ($('#yearForMonthDropdownHotWater').val()) + '-' + (($('#monthDropdownHotWater').val() * 1) + 1) + '-' + '01';
                $("#warmWaterStartDate-input").val(output1Lm);
                $("#warmWaterEndDate-input").val(output2Lm);
                loadHotWaterChart(isCompare);
                break;
            case "ly":
                $("#dateRangePannelWarmWater").hide();
                $("#monthlyPannelHotWater").show();
                $("#monthDropdownHotWater").hide();
                $("#yearDropdownHotWater").show();
                $("#yearForMonthDropdownHotWater").hide();
                var output1Ly = ($('#yearDropdownHotWater').val()) + '-' + '01' + '-' + '01';
                var output2Ly = (($('#yearDropdownHotWater').val() * 1) + 1) + '-' + '01' + '-' + '01';
                $("#warmWaterStartDate-input").val(output1Ly);
                $("#warmWaterEndDate-input").val(output2Ly);
                loadHotWaterChart(isCompare);
                break;
            default:
                $("#monthlyPannelHotWater").hide();
                $("#hotWaterCompairPanel").hide();
                $("#dateRangePannelWarmWater").show();
                var output1Def = (d.getFullYear() - 1) + '-' + (month < 10 ? '0' : '') + month + '-' + (day < 10 ? '0' : '') + day;
                var output2Def = d.getFullYear() + '-' + (month < 10 ? '0' : '') + month + '-' + (day < 10 ? '0' : '') + day;
                $("#warmWaterStartDate-input").val(output1Def);
                $("#warmWaterEndDate-input").val(output2Def);
                loadHotWaterChart(false);
                break;
        }
    });

    $('#hotWaterShowResultButton').click(function () {
        if ($("#warmWaterStartDate-input").val() != null || $("#warmWaterEndDate-input").val() != null) {
            loadHotWaterChart(false);
        }
    });

    function loadHotWaterChart(isCompare) {

        var sdate = $("#warmWaterStartDate-input").val();
        var edate = $("#warmWaterEndDate-input").val();
        $.ajax({
            type: "GET",
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            url: '/Dashboard/GetStatisticsByDateRange/',
            data: { sDate: sdate, eDate: edate, isCompare: isCompare, iSensorId: "4268" },
            success: function (data) {
                if (data.status == false) { window.location.href = '/Account/Login' }
                var seriesDataHotWater = data.data.Series;
                var categories = data.data.ColumnNameList;

                if (seriesDataHotWater.length == 0) {
                    $('#errorHotWater').show();
                    $('#container3').hide();
                } else {
                    $('#errorHotWater').hide();
                    $('#container3').show();
                }

                Highcharts.Color.prototype.parsers.push({
                    regex: /^[a-z]+$/,
                    parse: function (result) {
                        var rgb = new RGBColor(result[0]);
                        if (rgb.ok) {
                            return [rgb.r, rgb.g, rgb.b, 1]; // returns rgba to Highcharts
                        }
                    }
                });

                var chart = Highcharts.chart('container3',
                    {
                        chart: {
                            type: seriesDataHotWater[0].data.length < 30 ? 'column' : 'line'
                        },
                        colors: ['rgba(249, 65, 65, 0.3)', '#2D3F70'],
                        title: {
                            text: ""//isEnglish ? 'Warm Water Usage' : 'Varmvattenanvandning'
                        },
                        credits: {
                            enabled: false
                        },
                        yAxis: {
                            title: {
                                text: ' Liter '
                            },
                            labels: {
                                formatter: function () {
                                    return this.value;
                                }

                            }
                        },
                        xAxis: {
                            categories: categories,

                            labels: {
                                formatter: function () {
                                    var date;
                                    if (!isEnglish) {
                                        var arr = this.value.split(/[- :/]/)
                                        date = new Date(arr[0], arr[1] - 1, arr[2], arr[3], arr[4], arr[5]);
                                    } else {
                                        date = new Date(this.value);
                                    }

                                    if (date.getHours() == 0) {
                                        return months[date.getMonth()];
                                    } else {
                                        return months[date.getMonth()] + ' ' + date.getDate();
                                    }
                                }
                            }
                        },
                        series: seriesDataHotWater,
                        tooltip: {
                            formatter: function () {
                                var date;
                                if (!isEnglish) {
                                    var arr = this.x.split(/[- :/]/)
                                    date = new Date(arr[0], arr[1] - 1, arr[2], arr[3], arr[4], arr[5]);
                                } else {
                                    date = new Date(this.x);
                                }

                                if (date.getHours() == 0) {
                                    return ' <b>' + this.series.name + '</b> - <b>' + months[date.getMonth()] + '</b> - <b>' + this.y + '</b> Liter ';
                                } else {
                                    return ' <b>' + this.series.name + '</b> - <b>' + months[date.getMonth()] + ' ' + date.getDate() + '</b> - <b>' + this.y + '</b> Liter ';
                                }
                            }
                        }
                    });

            }
        });
    }

});
/* warm water chart ends here */

/* Floor Heat chart starts here */
$(function () {
    var isCompare = false;
    loadFloorHeatChart(isCompare);
    $("input[name='optradioFloorHeat']").change(function () {
        // Do something interesting here
        if ($("input[name='optradioFloorHeat']:checked").val() == "compare") {
            $('#floorHeatRadioReset').show();
            isCompare = true;
            loadFloorHeatChart(true);
        } else {
            $('#floorHeatRadioReset').hide();
            isCompare = false;
            loadFloorHeatChart(false);
        }
    });

    $("#monthDropdownFloorHeat").change(function () {
        var output1 = ($('#yearDropdownFloorHeat').val()) + '-' + $('#monthDropdownFloorHeat').val() + '-' + '01';
        var output2 = ($('#yearDropdownFloorHeat').val()) + '-' + (($('#monthDropdownFloorHeat').val() * 1) + 1) + '-' + '01';
        $("#floorHeatStartDate-input").val(output1);
        $("#floorHeatEndDate-input").val(output2);
        loadFloorHeatChart(isCompare);
    });

    $("#yearForMonthDropdownFloorHeat").change(function () {
        var output1 = ($('#yearForMonthDropdownFloorHeat').val()) + '-' + $('#monthDropdownFloorHeat').val() + '-' + '01';
        var output2 = ($('#yearForMonthDropdownFloorHeat').val()) + '-' + (($('#monthDropdownFloorHeat').val() * 1) + 1) + '-' + '01';
        $("#floorHeatStartDate-input").val(output1);
        $("#floorHeatEndDate-input").val(output2);
        loadFloorHeatChart(isCompare);
    });

    $("#yearDropdownFloorHeat").change(function () {
        var output1 = ($('#yearDropdownFloorHeat').val()) + '-' + $('#monthDropdownFloorHeat').val() + '-' + '01';
        var output2 = (($('#yearDropdownFloorHeat').val() * 1) + 1) + '-' + $('#monthDropdownFloorHeat').val() + '-' + '01';
        $("#floorHeatStartDate-input").val(output1);
        $("#floorHeatEndDate-input").val(output2);
        loadFloorHeatChart(isCompare);
    });

    $("#timePeriodDropdownFloorHeat").change(function () {

        var ddVal = $('#timePeriodDropdownFloorHeat').val();
        switch (ddVal) {
            case "lw":
                var lastWeek = new Date();
                lastWeek.setDate(lastWeek.getDate() - 7);
                lastWeek = new Date(lastWeek);
                var output1Ld = lastWeek.getFullYear() + '-' + (lastWeek.getMonth() < 10 ? '0' : '') + (lastWeek.getMonth() + 1) + '-' + (lastWeek.getDate() < 10 ? '0' : '') + lastWeek.getDate();
                var output2Ld = d.getFullYear() + '-' + (month < 10 ? '0' : '') + month + '-' + (day < 10 ? '0' : '') + day;
                $("#monthlyPannelFloorHeat").hide();
                $("#dateRangePannelFloorHeat").hide();
                $("#floorHeatStartDate-input").val(output1Ld);
                $("#floorHeatEndDate-input").val(output2Ld);
                loadFloorHeatChart(isCompare);
                break;
            case "lm":
                $("#dateRangePannelFloorHeat").hide();
                $("#monthlyPannelFloorHeat").show();
                $("#monthDropdownFloorHeat").show();
                $("#yearDropdownFloorHeat").hide();
                $("#yearForMonthDropdownFloorHeat").show();
                var output1Lm = ($('#yearForMonthDropdownFloorHeat').val()) + '-' + $('#monthDropdownFloorHeat').val() + '-' + '01';
                var output2Lm = ($('#yearForMonthDropdownFloorHeat').val()) + '-' + (($('#monthDropdownFloorHeat').val() * 1) + 1) + '-' + '01';
                $("#floorHeatStartDate-input").val(output1Lm);
                $("#floorHeatEndDate-input").val(output2Lm);
                loadFloorHeatChart(isCompare);
                break;
            case "ly":
                $("#dateRangePannelFloorHeat").hide();
                $("#monthlyPannelFloorHeat").show();
                $("#monthDropdownFloorHeat").hide();
                $("#yearDropdownFloorHeat").show();
                $("#yearForMonthDropdownFloorHeat").hide();
                var output1Ly = ($('#yearDropdownFloorHeat').val()) + '-' + '01' + '-' + '01';
                var output2Ly = (($('#yearDropdownFloorHeat').val() * 1) + 1) + '-' + '01' + '-' + '01';
                $("#floorHeatStartDate-input").val(output1Ly);
                $("#floorHeatEndDate-input").val(output2Ly);
                loadFloorHeatChart(isCompare);
                break;
            default:
                $("#monthlyPannelFloorHeat").hide();
                $("#floorHeatCompairPanel").hide();
                $("#dateRangePannelFloorHeat").show();
                var output1Def = (d.getFullYear() - 1) + '-' + (month < 10 ? '0' : '') + month + '-' + (day < 10 ? '0' : '') + day;
                var output2Def = d.getFullYear() + '-' + (month < 10 ? '0' : '') + month + '-' + (day < 10 ? '0' : '') + day;
                $("#floorHeatStartDate-input").val(output1Def);
                $("#floorHeatEndDate-input").val(output2Def);
                loadFloorHeatChart(false);
                break;
        }
    });

    $('#floorHeatShowResultButton').click(function () {
        if ($("#floorHeatStartDate-input").val() != null || $("#floorHeatEndDate-input").val() != null) {
            loadFloorHeatChart(false);
        }
    });

    function loadFloorHeatChart(isCompare) {

        var sdate = $("#floorHeatStartDate-input").val();
        //sdate.setMonth(sdate.getMonth() - 6);
        var edate = $("#floorHeatEndDate-input").val();

        $.ajax({
            type: "GET",
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            url: '/Dashboard/GetStatisticsByDateRange/',
            data: { sDate: sdate, eDate: edate, isCompare: isCompare, iSensorId: "1604" },
            success: function (data) {
                if (data.status == false) { window.location.href = '/Account/Login' }
                var seriesData = data.data.Series;
                var categories = data.data.ColumnNameList;

                if (seriesData.length == 0) {
                    $('#errorFloorHeat').show();
                    $('#container6').hide();
                } else {
                    $('#errorFloorHeat').hide();
                    $('#container6').show();
                }

                Highcharts.Color.prototype.parsers.push({
                    regex: /^[a-z]+$/,
                    parse: function (result) {
                        var rgb = new RGBColor(result[0]);
                        if (rgb.ok) {
                            return [rgb.r, rgb.g, rgb.b, 1]; // returns rgba to Highcharts
                        }
                    }
                });
                var chart = Highcharts.chart('container6',
                    {
                        chart: {
                            type: seriesData[0].data.length < 30 ? 'column' : 'line'
                        },
                        credits: {
                            enabled: false
                        },
                        colors: ['rgba(127, 164, 90, 0.3)', '#2D3F70'],
                        title: {
                            text: ""//isEnglish ? 'Floor Heat Usage' : 'Golvvarme'
                        },
                        yAxis: {
                            title: {
                                text: ' kWh '
                            },
                            labels: {
                                formatter: function () {
                                    return this.value;
                                }

                            }
                        },
                        xAxis: {
                            categories: categories,

                            labels: {
                                formatter: function () {
                                    var date;
                                    if (!isEnglish) {
                                        var arr = this.value.split(/[- :/]/)
                                        date = new Date(arr[0], arr[1] - 1, arr[2], arr[3], arr[4], arr[5]);
                                    } else {
                                        date = new Date(this.value);
                                    }

                                    if (date.getHours() == 0) {
                                        return months[date.getMonth()];
                                    } else {
                                        return months[date.getMonth()] + ' ' + date.getDate();
                                    }
                                }
                            }
                        },
                        series: seriesData,
                        tooltip: {
                            formatter: function () {
                                var date;
                                if (!isEnglish) {
                                    var arr = this.x.split(/[- :/]/)
                                    date = new Date(arr[0], arr[1] - 1, arr[2], arr[3], arr[4], arr[5]);
                                } else {
                                    date = new Date(this.x);
                                }

                                if (date.getHours() == 0) {
                                    return ' <b>' + this.series.name + '</b> - <b>' + months[date.getMonth()] + '</b> - <b>' + this.y + '</b> kWh ';
                                } else {
                                    return ' <b>' + this.series.name + '</b> - <b>' + months[date.getMonth()] + ' ' + date.getDate() + '</b> - <b>' + this.y + '</b> kWh ';
                                }
                            }
                        }

                    });
            }
        });
    }
});
/* Floor Heat chart ends here */

/* Heat chart starts here */
$(function () {
    var isCompare = false;
    loadHeatChart(isCompare);
    $("input[name='optradioHeat']").change(function () {
        // Do something interesting here
        if ($("input[name='optradioHeat']:checked").val() == "compare") {
            $('#heatRadioReset').show();
            isCompare = true;
            loadHeatChart(true);
        } else {
            $('#heatRadioReset').hide();
            isCompare = false;
            loadHeatChart(false);
        }

    });

    $("#monthDropdownHeat").change(function () {
        var output1 = ($('#yearDropdownHeat').val()) + '-' + $('#monthDropdownHeat').val() + '-' + '01';
        var output2 = ($('#yearDropdownHeat').val()) + '-' + (($('#monthDropdownHeat').val() * 1) + 1) + '-' + '01';
        $("#heatStartDate-input").val(output1);
        $("#heatEndDate-input").val(output2);
        loadHeatChart(isCompare);
    });

    $("#yearForMonthDropdownHeat").change(function () {
        var output1 = ($('#yearForMonthDropdownHeat').val()) + '-' + $('#monthDropdownHeat').val() + '-' + '01';
        var output2 = ($('#yearForMonthDropdownHeat').val()) + '-' + (($('#monthDropdownHeat').val() * 1) + 1) + '-' + '01';
        $("#heatStartDate-input").val(output1);
        $("#heatEndDate-input").val(output2);
        loadHeatChart(isCompare);
    });

    $("#yearDropdownHeat").change(function () {
        var output1 = ($('#yearDropdownHeat').val()) + '-' + $('#monthDropdownHeat').val() + '-' + '01';
        var output2 = (($('#yearDropdownHeat').val() * 1) + 1) + '-' + $('#monthDropdownHeat').val() + '-' + '01';
        $("#heatStartDate-input").val(output1);
        $("#heatEndDate-input").val(output2);
        loadHeatChart(isCompare);
    });

    $("#timePeriodDropdownHeat").change(function () {

        var ddVal = $('#timePeriodDropdownHeat').val();
        switch (ddVal) {
            case "lw":
                $("#monthlyPannelHeat").hide();
                $("#dateRangePannelHeat").hide();
                var lastWeek = new Date();
                lastWeek.setDate(lastWeek.getDate() - 7);
                lastWeek = new Date(lastWeek);
                var output1Ld = lastWeek.getFullYear() + '-' + (lastWeek.getMonth() < 10 ? '0' : '') + (lastWeek.getMonth() + 1) + '-' + (lastWeek.getDate() < 10 ? '0' : '') + lastWeek.getDate();
                var output2Ld = d.getFullYear() + '-' + (month < 10 ? '0' : '') + month + '-' + (day < 10 ? '0' : '') + day;
                $("#heatStartDate-input").val(output1Ld);
                $("#heatEndDate-input").val(output2Ld);
                loadHeatChart(isCompare);
                break;
            case "lm":
                $("#dateRangePannelHeat").hide();
                $("#monthlyPannelHeat").show();
                $("#monthDropdownHeat").show();
                $("#yearDropdownHeat").hide();
                $("#yearForMonthDropdownHeat").show();
                var output1Lm = ($('#yearForMonthDropdownHeat').val()) + '-' + $('#monthDropdownHeat').val() + '-' + '01';
                var output2Lm = ($('#yearForMonthDropdownHeat').val()) + '-' + (($('#monthDropdownHeat').val() * 1) + 1) + '-' + '01';
                $("#heatStartDate-input").val(output1Lm);
                $("#heatEndDate-input").val(output2Lm);
                loadHeatChart(isCompare);
                break;
            case "ly":
                $("#dateRangePannelHeat").hide();
                $("#monthlyPannelHeat").show();
                $("#monthDropdownHeat").hide();
                $("#yearDropdownHeat").show();
                $("#yearForMonthDropdownHeat").hide();
                var output1Ly = ($('#yearDropdownHeat').val()) + '-' + '01' + '-' + '01';
                var output2Ly = (($('#yearDropdownHeat').val() * 1) + 1) + '-' + '01' + '-' + '01';
                $("#heatStartDate-input").val(output1Ly);
                $("#heatEndDate-input").val(output2Ly);
                loadHeatChart(isCompare);
                break;
            default:
                $("#monthlyPannelHeat").hide();
                $("#heatCompairPanel").hide();
                $("#dateRangePannelHeat").show();
                var output1Def = (d.getFullYear() - 1) + '-' + (month < 10 ? '0' : '') + month + '-' + (day < 10 ? '0' : '') + day;
                var output2Def = d.getFullYear() + '-' + (month < 10 ? '0' : '') + month + '-' + (day < 10 ? '0' : '') + day;
                $("#heatStartDate-input").val(output1Def);
                $("#heatEndDate-input").val(output2Def);
                loadHeatChart(false);
                break;
        }
    });

    $('#heatShowResultButton').click(function () {
        if ($("#heatStartDate-input").val() != null || $("#heatEndDate-input").val() != null) {
            loadHeatChart(false);
        }
    });

    function loadHeatChart(isCompare) {

        var sdate = $("#heatStartDate-input").val();
        //sdate.setMonth(sdate.getMonth() - 6);
        var edate = $("#heatEndDate-input").val();

        $.ajax({
            type: "GET",
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            url: '/Dashboard/GetStatisticsByDateRange/',
            data: { sDate: sdate, eDate: edate, isCompare: isCompare, iSensorId: "1659" },
            success: function (data) {
                if (data.status == false) { window.location.href = '/Account/Login' }
                var seriesData = data.data.Series; // [data];// or [data]
                var categories = data.data.ColumnNameList; // [data];// or [data]
                //bindElectricityChart(seriesData, categories);
                if (seriesData.length == 0) {
                    $('#errorHeat').show();
                    $('#container4').hide();
                } else {
                    $('#errorHeat').hide();
                    $('#container4').show();
                }

                Highcharts.Color.prototype.parsers.push({
                    regex: /^[a-z]+$/,
                    parse: function (result) {
                        var rgb = new RGBColor(result[0]);
                        if (rgb.ok) {
                            return [rgb.r, rgb.g, rgb.b, 1]; // returns rgba to Highcharts
                        }
                    }
                });

                var chart = Highcharts.chart('container4',
                    {
                        chart: {
                            type: seriesData[0].data.length < 30 ? 'column' : 'line'
                        },
                        credits: {
                            enabled: false
                        },
                        colors: ['rgba(214, 1, 1, 0.3)', '#2D3F70'],
                        title: {
                            text: ""//isEnglish ? 'Heat Usage' : 'Varmemangd'
                        },
                        yAxis: {
                            title: {
                                text: ' kWh '
                            },
                            labels: {
                                formatter: function () {
                                    return this.value;
                                }

                            }
                        },
                        xAxis: {
                            categories: categories,

                            labels: {
                                formatter: function () {
                                    var date;
                                    if (!isEnglish) {
                                        var arr = this.value.split(/[- :/]/)
                                        date = new Date(arr[0], arr[1] - 1, arr[2], arr[3], arr[4], arr[5]);
                                    } else {
                                        date = new Date(this.value);
                                    }

                                    if (date.getHours() == 0) {
                                        return months[date.getMonth()];
                                    } else {
                                        return months[date.getMonth()] + ' ' + date.getDate();
                                    }
                                }
                            }
                        },
                        series: seriesData,
                        tooltip: {
                            formatter: function () {
                                var date;
                                if (!isEnglish) {
                                    var arr = this.x.split(/[- :/]/)
                                    date = new Date(arr[0], arr[1] - 1, arr[2], arr[3], arr[4], arr[5]);
                                } else {
                                    date = new Date(this.x);
                                }

                                if (date.getHours() == 0) {
                                    return ' <b>' + this.series.name + '</b> - <b>' + months[date.getMonth()] + '</b> - <b>' + this.y + '</b> kWh ';
                                } else {
                                    return ' <b>' + this.series.name + '</b> - <b>' + months[date.getMonth()] + ' ' + date.getDate() + '</b> - <b>' + this.y + '</b> kWh ';
                                }
                            }
                        }
                    });

            }
        });
    }
});
/* Heat chart ends here */

/* Temperature chart starts here */
$(function () {
    var isCompare = false;
    loadTemperatureChart(isCompare);
    $("input[name='optradioTemperature']").change(function () {
        // Do something interesting here
        if ($("input[name='optradioTemperature']:checked").val() == "compare") {
            $('#tempRadioReset').show();
            isCompare = true;
            loadTemperatureChart(true);
        } else {
            $('#tempRadioReset').hide();
            isCompare = false;
            loadTemperatureChart(false);
        }

    });

    $("#monthDropdownTemp").change(function () {
        var output1 = ($('#yearDropdownTemp').val()) + '-' + $('#monthDropdownTemp').val() + '-' + '01';
        var output2 = ($('#yearDropdownTemp').val()) + '-' + (($('#monthDropdownTemp').val() * 1) + 1) + '-' + '01';
        $("#temperatureStartDate-input").val(output1);
        $("#temperatureEndDate-input").val(output2);
        loadTemperatureChart(isCompare);
    });

    $("#yearForMonthDropdownTemp").change(function () {
        var output1 = ($('#yearForMonthDropdownTemp').val()) + '-' + $('#monthDropdownTemp').val() + '-' + '01';
        var output2 = ($('#yearForMonthDropdownTemp').val()) + '-' + (($('#monthDropdownTemp').val() * 1) + 1) + '-' + '01';
        $("#temperatureStartDate-input").val(output1);
        $("#temperatureEndDate-input").val(output2);
        loadTemperatureChart(isCompare);
    });

    $("#yearDropdownTemp").change(function () {
        var output1 = ($('#yearDropdownTemp').val()) + '-' + $('#monthDropdownTemp').val() + '-' + '01';
        var output2 = (($('#yearDropdownTemp').val() * 1) + 1) + '-' + $('#monthDropdownTemp').val() + '-' + '01';
        $("#temperatureStartDate-input").val(output1);
        $("#temperatureEndDate-input").val(output2);
        loadTemperatureChart(isCompare);
    });

    $("#timePeriodDropdownTemperature").change(function () {

        var ddVal = $('#timePeriodDropdownTemperature').val();
        switch (ddVal) {
            case "lw":
                $("#monthlyPannelTemp").hide();
                $("#dateRangePannelTemp").hide();
                var lastWeek = new Date();
                lastWeek.setDate(lastWeek.getDate() - 7);
                lastWeek = new Date(lastWeek);
                var output1Ld = lastWeek.getFullYear() + '-' + (lastWeek.getMonth() < 10 ? '0' : '') + (lastWeek.getMonth() + 1) + '-' + (lastWeek.getDate() < 10 ? '0' : '') + lastWeek.getDate();
                var output2Ld = d.getFullYear() + '-' + (month < 10 ? '0' : '') + month + '-' + (day < 10 ? '0' : '') + day;
                $("#temperatureStartDate-input").val(output1Ld);
                $("#temperatureEndDate-input").val(output2Ld);
                loadTemperatureChart(isCompare);
                break;
            case "lm":
                $("#dateRangePannelTemp").hide();
                $("#monthlyPannelTemp").show();
                $("#monthDropdownTemp").show();
                $("#yearDropdownTemp").hide();
                $("#yearForMonthDropdownTemp").show();
                var output1Lm = ($('#yearForMonthDropdownTemp').val()) + '-' + $('#monthDropdownTemp').val() + '-' + '01';
                var output2Lm = ($('#yearForMonthDropdownTemp').val()) + '-' + (($('#monthDropdownTemp').val() * 1) + 1) + '-' + '01';
                $("#temperatureStartDate-input").val(output1Lm);
                $("#temperatureEndDate-input").val(output2Lm);
                loadTemperatureChart(isCompare);
                break;
            case "ly":
                $("#dateRangePannelTemp").hide();
                $("#monthlyPannelTemp").show();
                $("#monthDropdownTemp").hide();
                $("#yearDropdownTemp").show();
                $("#yearForMonthDropdownTemp").hide();
                var output1Ly = ($('#yearDropdownTemp').val()) + '-' + '01' + '-' + '01';
                var output2Ly = (($('#yearDropdownTemp').val() * 1) + 1) + '-' + '01' + '-' + '01';
                $("#temperatureStartDate-input").val(output1Ly);
                $("#temperatureEndDate-input").val(output2Ly);
                loadTemperatureChart(isCompare);
                break;
            default:
                $("#monthlyPannelTemp").hide();
                $("#tempCompairPanel").hide();
                $("#dateRangePannelTemp").show();
                var output1Def = (d.getFullYear() - 1) + '-' + (month < 10 ? '0' : '') + month + '-' + (day < 10 ? '0' : '') + day;
                var output2Def = d.getFullYear() + '-' + (month < 10 ? '0' : '') + month + '-' + (day < 10 ? '0' : '') + day;
                $("#temperatureStartDate-input").val(output1Def);
                $("#temperatureEndDate-input").val(output2Def);
                loadTemperatureChart(false);
                break;
        }
    });

    $('#tempShowResultButton').click(function () {
        if ($("#temperatureStartDate-input").val() != null ||
            $("#temperatureEndDate-input").val() != null) {
            loadTemperatureChart(false);
        }
    });

    function loadTemperatureChart(isCompare) {

        var sdate = $("#temperatureStartDate-input").val();
        //sdate.setMonth(sdate.getMonth() - 6);
        var edate = $("#temperatureEndDate-input").val();

        $.ajax({
            type: "GET",
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            url: '/Dashboard/GetStatisticsByDateRange/',
            data: { sDate: sdate, eDate: edate, isCompare: isCompare, iSensorId: "586" },
            success: function (data) {
                if (data.status == false) { window.location.href = '/Account/Login' }
                var seriesData = data.data.Series; // [data];// or [data]
                var categories = data.data.ColumnNameList; // [data];// or [data]
                //bindElectricityChart(seriesData, categories);
                if (seriesData.length == 0) {
                    $('#errorTemperature').show();
                    $('#container5').hide();
                } else {
                    $('#errorTemperature').hide();
                    $('#container5').show();
                }

                Highcharts.Color.prototype.parsers.push({
                    regex: /^[a-z]+$/,
                    parse: function (result) {
                        var rgb = new RGBColor(result[0]);
                        if (rgb.ok) {
                            return [rgb.r, rgb.g, rgb.b, 1]; // returns rgba to Highcharts
                        }
                    }
                });

                var chart = Highcharts.chart('container5',
                    {
                        chart: {
                            type: seriesData[0].data.length < 30 ? 'column' : 'line'
                        },
                        credits: {
                            enabled: false
                        },
                        colors: ['rgba(244,136,73,0.3)', '#2D3F70'],
                        title: {
                            text: ""//isEnglish ? 'Temperature Usage' : 'Temperatur'
                        },
                        yAxis: {
                            title: {
                                text: 'C'
                            },
                            labels: {
                                formatter: function () {
                                    return this.value;
                                }

                            }
                        },
                        xAxis: {
                            categories: categories,

                            labels: {
                                formatter: function () {
                                    var date;
                                    if (!isEnglish) {
                                        var arr = this.value.split(/[- :/]/)
                                        date = new Date(arr[0], arr[1] - 1, arr[2], arr[3], arr[4], arr[5]);
                                    } else {
                                        date = new Date(this.value);
                                    }

                                    if (date.getHours() == 0) {
                                        return months[date.getMonth()];
                                    } else {
                                        return months[date.getMonth()] + ' ' + date.getDate();
                                    }
                                }
                            }
                        },
                        series: seriesData,
                        tooltip: {
                            formatter: function () {
                                var date;
                                if (!isEnglish) {
                                    var arr = this.x.split(/[- :/]/)
                                    date = new Date(arr[0], arr[1] - 1, arr[2], arr[3], arr[4], arr[5]);
                                } else {
                                    date = new Date(this.x);
                                }

                                if (date.getHours() == 0) {
                                    return ' <b>' + this.series.name + '</b> - <b>' + months[date.getMonth()] + '</b> - <b>' + this.y + '</b> C ';
                                } else {
                                    return ' <b>' + this.series.name + '</b> - <b>' + months[date.getMonth()] + ' ' + date.getDate() + '</b> - <b>' + this.y + '</b> C ';
                                }
                            }
                        }
                    });

            }
        });
    }
});
/* Temperature chart ends here */

